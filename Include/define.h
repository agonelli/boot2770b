/* 
 * File:   define.h
 * Author: agonelli
 *
 * Created on 22 maggio 2015, 9.39
 */

#ifndef DEFINE_H
#define	DEFINE_H

#ifdef	__cplusplus
extern "C" {
#endif

    //inirizzi LCD
#define RIGA1		0x00
#define RIGA2		0x40
#define RIGA3		20
#define RIGA4		84    
    

//costanti per printwlcd ED EDITW
#define	KG			0x01
#define GR			0x02
#define FSTESSO		0x04
#define	TOPRN			0x08
#define NOZERO		0x10
#define NEGATIVO 	0x20
#define ALIGNDX		0x40
#define FTUTTOEN	0x80
////********************************
////per lettura stringhe da ee
#define ABSADD		1
#define LANGADD		0
#define NOCR		2
#define NOSPACE         4
//

//getkey
#define WAITNOKEY	1
#define NOAUTOEXIT  2

//costanti per readNetweight
#define APPROX10	1
#define APPROX5 	2
#define NOTARA		4
//***************************************

//******* 21b0 usb *******
#define ADC_BUFLEN	 64
#define PRESS_ADC_BUFLEN	 128
#define PRESSM_ADC_BUFLEN	 16

#define CHGAS		0x01
#define CHSO 		0x02	//scarico olio
#define CHIO 		0x04	//iniezione olio
#define CHPRESS		0x08
#define CHUV		0x10

// min&max
#define GASMIN 		5

#define VERSIONEHW "3.0.1"
#define VERSIONEFWA "3.0.5"
#define VERSIONEFWB "3.0.5"

#define LEN_strGAS                  8

// costanti per repeattimer
#define REPEATTIME 30
#define REPEATCONT 10

//****************************************************************************************************
//***************** f l a s h    e s t e r n a ******************************************************
#define SFADD_LINGUA				0x0						//questo indirizzo deve rimanere sempre Zero perch� in tutto il programma per l'indirizzo
#define SF_LINGUA_LEN				SFMX25L_32_BlockSize            //0x10000
#define SFADD_OFFSETDB				(SFADD_LINGUA + SF_LINGUA_LEN)  //0x10000

#define DBLEN                       0x40000
#define SFLASHADDDB1                (SFADD_LINGUA + SF_LINGUA_LEN)  //0x10000
#define SFLASHADDDB2                (SFLASHADDDB1 + DBLEN)          //0x50000

#define SF_OFFSETDB_LEN				0x400
#define SFADD_OFFSETDBMODELLI		(SFADD_OFFSETDB + SF_OFFSETDB_LEN)
#define SF_OFFSETDBMODELLI_LEN		(SFMX25L_32_BlockSize * 8) - SF_OFFSETDB_LEN

#define SFADD_PROG                  (SFLASHADDDB2+DBLEN)	//0x90000		(SFADD_OFFSETDBMODELLI + SF_OFFSETDBMODELLI_LEN)
#define SF_PROG_LEN					SFMX25L_32_BlockSize    //0x10000

#define SDADD_PARAMETRI				SFADD_PROG + SF_PROG_LEN//0xa0000
#define SF_PARAMETRI_LEN			SFMX25L_32_BlockSize    //0x10000    

#define SFADD_GASPAR				SDADD_PARAMETRI + SF_PARAMETRI_LEN  //0xb0000

#define LEN_strGAS                  8
#define OFFSETGAS_1_tubo            LEN_strGAS
#define OFFSETGAS_2_tubi            (OFFSETGAS_1_tubo + sizeof(short))
#define OFFSETGAS_TEMP              (OFFSETGAS_2_tubi + sizeof(short))
#define GAS_NofTemp                 108
#define LEN_GAS                     (OFFSETGAS_TEMP + GAS_NofTemp * sizeof(short))


#define SF_GASPAR_LEN				SFMX25L_32_BlockSize

#define SFADD_RECORD				SFADD_GASPAR + SF_GASPAR_LEN    //0xc0000
#define SF_RECORD_LEN				(SFMX25L_32_BlockSize * 2)

#define SFADD_FILTRI				SFADD_RECORD + SF_RECORD_LEN    //0xd0000
#define SF_FILTRI_LEN				SFMX25L_32_BlockSize

#define SFADD_LINGUA2				SFADD_FILTRI + SF_RECORD_LEN    //0xe0000
#define SF_LINGUA_LEN				SFMX25L_32_BlockSize    //0x10000
    
#define SFADD_SAFEPROG 				SFMX25L_32_Size - SFMX25L_32_BlockSize
#define SF_SAFEPROG_LEN				SFMX25L_32_BlockSize


//flags1
#define B1S		0x01
#define B100MS	0x02
#define B10MS	0x04
#define REFDISP 0x08
#define FBIP1	0x10
#define FBIP2   0x20
#define DISOUTNC 0x40
//#define FAUTO   0x80

//flags2
#define F1S			0x01
#define INTSYNC10MS 0x02
#define F100MS		0x04
//#define FPOLLING	0x08
#define B1M			0x10
#define F500MS		0x20
#define FBL			0x40
#define B1H			0x80

//flags3
#define FLP			0x01
#define FHP			0x02
#define HPLPMISTO1  0x04
#define HPLPMISTO2	0x08
#define FTUTTO		0x10

//inputs
#define VA18 	0x02
#define PV15 	0x04
#define P_MAX	0x08

//out
#define OUTR	0x01
#define OUTV	0x02
#define OUTC	0x04
#define OUTIO	0x08
#define OUTSO	0x10
#define OUTUV	0x20
#define OUTNC	0x40


//***** led  *****
#define LA	0x01
#define LR	0x02
#define LV	0x04
#define LC	0x08

#define BUZ	0x40

#define KUPDOWN			0x22
#define KLAVAGGIO	    0x81
#define KSTARTUPDOWNESC	0x25
#define KSTOPFASI		0x26
#define KSTARTSTOPENTER 0x27

#define KEXIT	        0x80

#define KFASI	        0xfd
//#define KNUM	        0xfe
#define KNULL	        0xff

#define TARATURA_MEDIE	8
#define TARATURA_KM		8192

#define ENTIREYEAR  1


//db interno
#define EDIT	0x01
#define BROWSE	0x02

//costanti per cicli
#define LAVAGGIOSCARICOOLIO 0x10
#define CARICATUTTO	8
//#define QREC	4
#define LAVAGGIO 2
#define AUTO	1
#define MANU	4
#define ALARM	0x20

#define FASEOLIO	0x10
#define FASEUV		0x20
#define	FASERICARICA 0x40

#define DELAYPMAX	20

//****************************************************************************************************

#define PASSWORDPARAMETRI 	    1234
#define PASSWORDCONTPARZ	    5111
#define PASSWORDCONTTOT		    2111
#define PASSWORDDEFAULTS	    1243
#define PASSWORDCOLLAUDO	    2442
#define PASSWORDUPGRADE             1237
#define PASSWORDZEROCELLE	    2376
#define PASSWORDTESTOUT		    2445
#define PASSWORDTESTADC		    2455
#define PASSWORDVISLINGUE           7777
#define PASSWORDCALIB   	    6234
#define PASSWORDEDITEE   	    5271
#define PASSWORDRESET   	    1757
#define PASSWORDCLRRECORD	    1732
#define PASSWORDUPDATELINGUA        1733
#define PASSWORDUPDATEDB	    6633
#define PASSWORDUPDATEPAR	    1772
#define PASSWORDUPDATEGAS	    6655
#define PASSWORDSAVEEE              4321
#define PASSWORDLOADEE              4322
#define PASSWORDRESETFILTRI	    5454
#define PASSWORDCHECK		    5252

#define ELFASECRET      	    1212

#define PARZ	2
#define TOT		4

#define TARGAAUTO MAINMENU

#define CARICATUTTO	8

//taratura
#define TARATURA_ZERO_E_COEFF	0
#define TARATURA_SOLO_ZERO  1

#define MAX_NofFilter 10

#define ADD_SECOND_GROUP	0x100

#define EDITALFA_FILTRI 1
#define EDITALFA_NORM   0

#define TMP_BUF_SIZE   128

#define FilterLen 8

#define Lang_File "21b0sdlg.bin"
#define Lang_File2 "21b0sdl2.bin"
#define Param_File "21b0sdpa.bin"
//#define DB_File "21b0sddb.bin"
#define DB_File1 "DB1234.bin"
#define DB_File2 "DB134.bin"
#define DB_File3 "WDB1234.bin"
#define DB_File4 "WDB134.bin"
#define GAS_File "21b0sdga.bin"

#define NOCR		2
#define NOSPACE     4

#define ADDENDPRINT1    0x0380	//righe dati fine stampa
#define ADDENDPRINT2    0x0390	//righe dati fine stampa
#define ADDENDPRINT3    0x03a0	//righe dati fine stampa
#define ADDENDPRINT4    0x03b0	//righe dati fine stampa
#define ADDENDPRINT5    0x03c0	//righe dati fine stampa
#define ADDENDPRINT6    0x03d0	//righe dati fine stampa

#define STRPRIMI		200	//rvc

#define MAXOLIO	999
#define MAXUV	999

#define TRTNCREC 150
    
#ifdef	__cplusplus
}
#endif

#endif	/* DEFINE_H */

