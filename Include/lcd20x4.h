#ifndef _DECL_LCD20X4
#define _DECL_LCD20X4 extern


#endif


_DECL_LCD20X4 void waitbflcd(void);
//_DECL_LCD20X4 void setCursor(unsigned char add,unsigned char flags);
_DECL_LCD20X4 void printwlcd(unsigned char add,unsigned short w,unsigned char nchar,unsigned char flags);
//_DECL_LCD20X4 void printdwlcd(unsigned char add,unsigned int dw,unsigned char nchar,unsigned char flags);
_DECL_LCD20X4 void printlcd(unsigned char add,char * p);
_DECL_LCD20X4 void writelcd(unsigned char flcd,unsigned char data);
_DECL_LCD20X4 void clrlcd(void);
_DECL_LCD20X4 void initlcd(void);
_DECL_LCD20X4 void printD(unsigned char add,unsigned short w);
_DECL_LCD20X4 void printFlashLCD(word add,byte lcdadd,byte nchar,byte flag);
//_DECL_LCD20X4 void setSymbolLCD(void);
_DECL_LCD20X4 void barV(unsigned char val,unsigned char max );
_DECL_LCD20X4 void screenshot(unsigned short riga1,unsigned char opt1,unsigned char add1,unsigned short riga2,unsigned char opt2,unsigned char add2,unsigned short riga3,unsigned char opt3,unsigned char add3,unsigned short riga4,unsigned char opt4,unsigned char add4);
_DECL_LCD20X4 void printCharLcd(byte add,char c);

//lcdreg
#define LCDRS	0x02
#define LCDEN	0x40
#define LCDRW	0x01

//costanti per printwlcd
#define KG		0x1
//#define BIT9		0x2
#define TRIMZERO	0x04
//#define TOPRN		8
#define NOZERO		0x10
#define NEGATIVO 	0x20
#define ALIGNDX		0x40
//#define FVDMAX		0x80	definito altrove
#define FVUOTO		0X100

#define NOOFFSET        0
#define UDMKG           0x40
#define UDMG            0x80
#define UDMMBAR         0xc0


