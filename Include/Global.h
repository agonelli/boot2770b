/* 
 * File:   global.h
 * Author: andrea
 *
 */

#ifndef GLOBAL_H
#define	GLOBAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "HWGPIO.h"
#include "Utils\RTCUtil.h"
#include "FlashDriver/sflashMX25L.h"
#include "define.h"
#include "typedef.h"

//******************************************************************************************************************************
//******************************************************************************************************************************
//******************************************************************************************************************************
// VARIABILI GLOBALI ***********************************************************************************************************
//******************************************************************************************************************************
//******************************************************************************************************************************
//******************************************************************************************************************************
#ifndef GLOBAL
#define GLOBAL extern
extern const char Str16Blanck[];
extern const char strSTART[];
extern const char strSKIP[];
extern const char strOK[];
extern const char strALARM[];
extern const char strPRIMI[];
GLOBAL byte contlv;
GLOBAL word sec_timer1w;
GLOBAL word sec_timer2w_inc;
GLOBAL dword sec_timer3dw;
GLOBAL byte min_timer0;
GLOBAL volatile byte cont10ms;
GLOBAL volatile byte cont100ms;
GLOBAL volatile byte cont500ms1;
GLOBAL volatile byte cont1s;
GLOBAL volatile byte cont1m;
GLOBAL volatile byte cont1h;
GLOBAL volatile byte trefdisp;
GLOBAL volatile byte rtrefdisp;
GLOBAL volatile byte rtncrec;
GLOBAL volatile byte adcflags;
GLOBAL volatile word oiltimer;
GLOBAL char Targa[17];
#else //**********************************************************************************************************************
//#define GLOBAL
const char Str16Blanck[] = "                ";
const char strSTART[] = "START to confirm";
const char strSKIP[] = " SKIP";
const char strOK[] = " OK";
const char strALARM[] = " ALARM";
const char strPRIMI[] ="'";
GLOBAL byte contlv=1;
GLOBAL word sec_timer1w=1;
GLOBAL word sec_timer2w_inc=1;
GLOBAL dword sec_timer3dw=1;
GLOBAL byte min_timer0=1;
GLOBAL volatile byte cont10ms=1;
GLOBAL volatile byte cont100ms=1;
GLOBAL volatile byte cont500ms1=1;
GLOBAL volatile byte cont1s=1;
GLOBAL volatile byte cont1m=1;
GLOBAL volatile byte cont1h=30;
GLOBAL volatile byte trefdisp=5;
GLOBAL volatile byte rtrefdisp=1;
GLOBAL volatile byte rtncrec=0;
GLOBAL volatile byte adcflags=0;
GLOBAL volatile word oiltimer=0;
GLOBAL char Targa[17]={' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0};
#endif
//*** variabili globali non inizializzate /**************************
//************* 21b0usb **********
GLOBAL volatile byte bufcount;
GLOBAL byte tempBuf[8*MAX_NofFilter+1];
GLOBAL byte autoprint;
GLOBAL volatile byte adcbufidxGAS;
GLOBAL volatile byte adcbufidxSO;
GLOBAL volatile byte adcbufidxIO;
GLOBAL volatile byte adcbufidxUV;
GLOBAL volatile byte adcbufidxPRESS;
GLOBAL volatile byte adcbufidxPRESSM;
GLOBAL volatile word bufGAS[ADC_BUFLEN];
GLOBAL volatile word bufIO[ADC_BUFLEN];	//iniezione olio
GLOBAL volatile word bufUV[ADC_BUFLEN];	//iniezione UV
GLOBAL volatile word bufSO[ADC_BUFLEN];	//scarico olio
GLOBAL volatile word bufPRESS[PRESS_ADC_BUFLEN];
GLOBAL volatile word bufPRESSM[PRESSM_ADC_BUFLEN];
GLOBAL word zero[5];
GLOBAL word kConv[5];
GLOBAL byte adcench;	//abilitazione canali
GLOBAL byte adcattch;	//canale attuale
GLOBAL Ver21B0 gVer;
GLOBAL byte out;
GLOBAL byte led;
GLOBAL volatile word sec_timer0w;
GLOBAL volatile byte timerpress;
GLOBAL volatile word convcont;
GLOBAL RTCTime RTC;
GLOBAL volatile byte key;
GLOBAL volatile byte flags1;
GLOBAL volatile byte flags2;
GLOBAL volatile byte flags3;
GLOBAL volatile byte rt0;
GLOBAL volatile byte rt1;
GLOBAL volatile byte rt2;
GLOBAL volatile byte rt3;
GLOBAL volatile byte rt4;
GLOBAL volatile byte repeatcont;
GLOBAL volatile byte esctimer;
GLOBAL volatile byte autoexittimer;
GLOBAL byte standbypage;
GLOBAL byte StandbyItems[5];
GLOBAL byte nStandbyItems;
GLOBAL byte bled;
GLOBAL byte bledv;
GLOBAL byte FiltriEsauriti;
GLOBAL byte udm;
GLOBAL byte fmenuref;
GLOBAL byte autoexit;
GLOBAL byte entst;
GLOBAL char strGAS[9];
GLOBAL byte lingua;
GLOBAL char tmpString[40];
GLOBAL byte tstbuf[4];
GLOBAL byte tstidx;
GLOBAL volatile byte tst;
GLOBAL byte prevtst;
GLOBAL byte repeattimer;
GLOBAL byte freecounter;
GLOBAL volatile byte conthp;
GLOBAL byte prothp;
GLOBAL byte ClockEnable;
GLOBAL word contaminuti;
GLOBAL byte runcontaminuti;
GLOBAL byte olioMan;
GLOBAL byte CellaOlioIn;
GLOBAL byte CellaUvIn;
GLOBAL byte CellaOlioOut;
GLOBAL byte fSkipTestVuoto;
GLOBAL byte fStessoOlio;
GLOBAL t_Lavaggio Lavaggio;
GLOBAL t_Vuoto Vuoto;
GLOBAL t_Recupero Recupero;
GLOBAL t_Carica Carica;
GLOBAL Tubi gTubi;
GLOBAL char g_cTmpBuf[TMP_BUF_SIZE];
GLOBAL word maxGas;
GLOBAL byte StampanteAbilitata;
GLOBAL word tempw0;
GLOBAL dword tempdw0;
GLOBAL byte repeattime;
GLOBAL char gpbuf[64];
GLOBAL byte ContaCicliLavaggio;
GLOBAL byte nCicliLavaggio;
GLOBAL byte gasType;
GLOBAL dword sflashadddb;



//****** costanti controllo cursore ******
#define DISPON		0x04
#define CURON		0x02
#define BLINKON		0x01


//****** LED **************
#define	LD1		0x01 
#define LD2		0x02
#define LD3    	0x04
#define	LD4		0x08
#define	LDEX1	0x10
#define	LDEX2	0x20
//#define	LDEX3	0x40
#define LEDUSB    0x40
#define LCDON     0x80




#define RXBUFFERSIZE 520

#define FILENAME            0x3f0
#define FLAGBOOT            0x3fE

//flags1
#define B1S		0x01
#define B100MS	0x02
#define B10MS	0x04
#define REFDISP 0x08
#define FBIP1	0x10
#define FBIP2   0x20
#define DISOUTNC 0x40
#define FIOINT   0x80


#ifdef	__cplusplus
}
#endif

#endif	/* GLOBAL_H */


