#ifndef _DECL_LCDALFA
#define _DECL_LCDALFA extern


#endif


_DECL_LCDALFA void waitbflcd(void);
_DECL_LCDALFA void setCursor(unsigned char add,unsigned char flags);
_DECL_LCDALFA void printwlcd(unsigned char add,unsigned short w,unsigned char nchar,unsigned char flags);
_DECL_LCDALFA void printwlcdhex(byte add,word w,byte nchar) ;
_DECL_LCDALFA void printdwlcd(byte add,long dw,byte nchar,byte flags);
_DECL_LCDALFA void printlcd(unsigned char add,char * p);
_DECL_LCDALFA void writelcd(unsigned char flcd,unsigned char data);
_DECL_LCDALFA void clrlcd(void);
_DECL_LCDALFA void initlcd(void);
//_DECL_LCDALFA void printD(unsigned char add,unsigned short w);
//_DECL_LCDALFA void printFlashLCD(word add,byte lcdadd,byte nchar,byte flag);
_DECL_LCDALFA void printFlashLCD(word flashadd,byte lcdadd,byte nchar,byte lingua);//void printFlashLCD(word flashadd,byte lcdadd,byte flag);
_DECL_LCDALFA void setSymbolLCD(void);
//_DECL_LCDALFA void barV(unsigned char val,unsigned char max );
//_DECL_LCDALFA void barH(unsigned char add,long min,long max,long val );
_DECL_LCDALFA void screenshot(unsigned short riga1,unsigned char opt1,unsigned char add1,unsigned short riga2,unsigned char opt2,unsigned char add2,unsigned short riga3,unsigned char opt3,unsigned char add3,unsigned short riga4,unsigned char opt4,unsigned char add4);
_DECL_LCDALFA void printCharLcd(byte add,char c);
_DECL_LCDALFA void bar(byte add,word min, word max,word val,byte flags);
_DECL_LCDALFA void FillStrw(char * buf, word w, byte nchar, byte flags);
_DECL_LCDALFA dword approx10(dword dw);
_DECL_LCDALFA void printTimeLCD(byte add);
_DECL_LCDALFA void printDateLCD(byte add, byte flags);
_DECL_LCDALFA void FillStrFlash(char * buf, word flashadd, byte flag);
_DECL_LCDALFA void printBuflcd(byte add,const char * p, byte len);
_DECL_LCDALFA void printFlashsws(word flashadd1,word w,word flashadd2);
_DECL_LCDALFA void printFlashsc(word flashadd,byte * c);
//_DECL_LCDALFA void printFlashDbLCD(dword flashadd,byte lcdadd,byte nc);
_DECL_LCDALFA void FillStrDate(char * buf, byte flags);
_DECL_LCDALFA void FillStrTime(char * buf);
_DECL_LCDALFA void FillStrdw(char * buf, long dw, byte nchar, byte flags);

//lcdreg
#define LCDRS	0x02
#define LCDEN	0x40
#define LCDRW	0x01

//costanti per printwlcd
//costanti per printwlcd ED EDITW
//#define FTUTTOEN	0x01
//#define GR			0x02
//#define FSTESSO		0x04
////#define	TOPRN			0x08
//#define NOZERO		0x10
//#define NEGATIVO 	0x20
//#define ALIGNDX		0x40
//#define	KG			0x80
//********************************

//#define NOOFFSET        0
#define UDMG            0x80
#define UDMKG           0x40
#define UDMORE          0x20
#define UDMMINUTI       0x10
#define UDMSECONDI      0x08
#define UDMMBAR         0x04
#define UDMBAR          0x02

#define OFFSET1     0x80
#define BARVPRESENT     0x20

#define CHFrecciaGiu	0xC6
#define CHFrecciaSU     0xC5
#define CHQuadratoVuoto	0xFC
#define CHFrecciaSemplice_Destra 0xC7

//bar
#define NOPERC	0x01

typedef enum {
	CUSTSIMB_BOMBOLA_GAS,
	CUSTSIMB_FRECCIA_SU_RIGA,
	CUSTSIMB_FRECCIA_GIU_RIGA,
	CUSTSIMB_5_1,
	CUSTSIMB_5_2,
	CUSTSIMB_5_3,
	CUSTSIMB_5_4,
	CUSTSIMB_5_5,
	NofLCD_simboli_custom
}LCD_simboli_custom;

