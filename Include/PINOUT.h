/* 
 * File:   PINOUT.h
 * Author: andrea
 *
 * Created on 11 marzo 2013, 10.26
 */

#ifndef PINOUT_H
#define	PINOUT_H

#include <plib.h>


#ifdef	__cplusplus
extern "C" {
#endif

#define D0_PORTID IOPORT_E
#define D0_PIN BIT_0
#define D1_PORTID IOPORT_E
#define D1_PIN BIT_1
#define D2_PORTID IOPORT_E
#define D2_PIN BIT_2
#define D3_PORTID IOPORT_E
#define D3_PIN BIT_3
#define D4_PORTID IOPORT_E
#define D4_PIN BIT_4
#define D5_PORTID IOPORT_E
#define D5_PIN BIT_5
#define D6_PORTID IOPORT_E
#define D6_PIN BIT_6
#define D7_PORTID IOPORT_E
#define D7_PIN BIT_7
#define LCD_PORTID IOPORT_E
#define LCDBUSPINS  0xff
#define BUSYBIT 0x80
#define FCLOCK_PORTID IOPORT_G
#define FCLOCK_PIN BIT_6
#define FMISO_PORTID IOPORT_G
#define FMISO_PIN BIT_7
#define FMOSI_PORTID IOPORT_G
#define FMOSI_PIN BIT_8
#define FCS_PORTID IOPORT_G
#define FCS_PIN BIT_9
#define LCDCS1_PORTID  IOPORT_B
#define LCDCS1_PIN BIT_5
#define LCDCS2_PORTID  IOPORT_B
#define LCDCS2_PIN BIT_4
#define U3RX_PORTID  IOPORT_B
#define U3RX_PIN BIT_3
#define LCDCS3_PORTID  IOPORT_B
#define LCDCS3_PIN BIT_2
#define TEMPINT_PORTID  IOPORT_B
#define TEMPINT_PIN BIT_1
#define LED3EXT_PORTID  IOPORT_B
#define LED3EXT_PIN BIT_0
#define LCDRST_PORTID  IOPORT_B
#define LCDRST_PIN BIT_6
#define BUZZER_PORTID  IOPORT_B
#define BUZZER_PIN BIT_7
#define DIR485_PORTID  IOPORT_B
#define DIR485_PIN BIT_8
#define U3TX_PORTID  IOPORT_B
#define U3TX_PIN BIT_9
#define REFOUT_PORTID  IOPORT_B
#define REFOUT_PIN BIT_10
#define USBPWR_PORTID  IOPORT_B
#define USBPWR_PIN BIT_11
#define ENC1_PORTID  IOPORT_B
#define ENC1_PIN BIT_12
#define ENC2_PORTID  IOPORT_B
#define ENC2_PIN BIT_13
#define ENTST_PORTID  IOPORT_B
#define ENTST_PIN BIT_14
#define LCDRS_PORTID  IOPORT_B
#define LCDRS_PIN BIT_15
#define I2CSDA_PORTID IOPORT_F
#define I2CSDA_PIN BIT_4
#define I2CSCL_PORTID IOPORT_F
#define I2CSCL_PIN BIT_5
#define HWID_PORTID IOPORT_F
#define HWID_PIN BIT_3
#define OSC1_PORTID  IOPORT_C
#define OSC1_PIN BIT_12
#define OSC2_PORTID  IOPORT_C
#define OSC2_PIN BIT_15
#define LED4_PORTID  IOPORT_D
#define LED4_PIN BIT_8
#define LED3_PORTID  IOPORT_D
#define LED3_PIN BIT_9
#define LED1_PORTID  IOPORT_D
#define LED1_PIN BIT_10
#define LED1EXT_PORTID  IOPORT_D
#define LED1EXT_PIN BIT_11
#define RTS_PORTID  IOPORT_D
#define RTS_PIN BIT_0
#define LED2EXT_PORTID  IOPORT_C
#define LED2EXT_PIN BIT_13
#define CTS_PORTID  IOPORT_C
#define CTS_PIN BIT_14
#define USBOVERCUURENT_PORTID  IOPORT_D
#define USBOVERCUURENT_PIN BIT_1
#define U1RX_PORTID  IOPORT_D
#define U1RX_PIN BIT_2
#define U1TX_PORTID  IOPORT_D
#define U1TX_PIN BIT_3
#define LCDEN_PORTID  IOPORT_D
#define LCDEN_PIN BIT_4
#define LCDRW_PORTID IOPORT_D
#define LCDRW_PIN  BIT_5
#define LED2_PORTID  IOPORT_D
#define LED2_PIN BIT_6
#define LCDON_PORTID  IOPORT_D
#define LCDON_PIN BIT_7
#define U2TX_PORTID IOPORT_F
#define U2TX_PIN BIT_0
#define U2RX_PORTID IOPORT_F
#define U2RX_PIN BIT_1




#ifdef	__cplusplus
}
#endif

#endif	/* PINOUT_H */

