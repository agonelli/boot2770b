/********************************************************************
FileName:     usb_host_bootloader.c
Dependencies: See INCLUDES section
Processor:		PIC32 USB MSD Microcontrollers
Hardware:		
Complier:  	Microchip C18 (for PIC18), C30 (for PIC24), C32 (for PIC32)
Company:		Microchip Technology, Inc.
*******************************************************************/
#define GLOBAL
#include "global.h"
#include "MDD File System/FSIO.h"
#include "NVMem.h"

#include "Bootloader.h"
#include <plib.h>
#include "HardwareProfile.h"

#include "HWGPIO.h"
#include "HWTIMER.h"
#include "../HWInterface/hw_flash.h"
#include "GenericTypeDefs.h"
//#include "protocolUtils.h"
#include "lcdalfa.h"

// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
#define ADD_FILE_LANG   0
#define ADD_FILE_FWA    0x100000
#define ADD_FILE_FWB    0x200000
#define ADD_FILE_DB     0x300000

#define ADDFLASHEND     0x100000// *****************************************************************************
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *****************************************************************************
// Device Configuration Bits (Runs from Aux Flash)
// *****************************************************************************
#ifdef OMIT_CONFIGURATIONS
#else
#   pragma config UPLLEN   = ON            // USB PLL Enabled
#   pragma config FPLLMUL  = MUL_20        // PLL Multiplier
#   pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
#   pragma config FPLLIDIV = DIV_4//DIV_2         // PLL Input Divider
#   pragma config FPLLODIV = DIV_1//DIV_2         // PLL Output Divider
#   pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#   pragma config FWDTEN   = OFF           // Watchdog Timer
#   pragma config WDTPS    = PS1           // Watchdog Timer Postscale
//#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
#   pragma config OSCIOFNC = OFF           // CLKO Enable
#   pragma config POSCMOD  = HS            // Primary Oscillator
#   pragma config IESO     = OFF           // Internal/External Switch-over
#   pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable (KLO was off)
#   pragma config FNOSC    = PRIPLL        // Oscillator Selection
#   pragma config CP       = OFF           // Code Protect
#   pragma config BWP      = OFF           // Boot Flash Write Protect
//   #pragma config PWP      = OFF           // Program Flash Write Protect
#   pragma config ICESEL   = ICS_PGx1      // ICE/ICD Comm Channel Select


// *****************************************************************************
// DEVCFG3
// USERID = No Setting
#   pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#   pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#   pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#   pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)

#   if defined(TRANSPORT_LAYER_ETH)
#       pragma config FMIIEN = OFF, FETHIO = OFF	// external PHY in RMII/alternate configuration	
#   endif

#   if defined(__PIC32MX1XX_2XX__)
#   elif defined(__PIC32MX3XX_7XX__)
                             // For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx 
                             // devices the ICE connection is on PGx2. .
#       pragma config ICESEL = ICS_PGx2    // ICE pins configured on PGx2, Boot write protect OFF.
                             //For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx devices, 
                             //the output divisor is set to 1 to produce max 80MHz clock.
#       pragma config FPLLODIV = DIV_1         // PLL Output Divider: Divide by 1
#   endif
#endif

/******************************************************************************
Macros used in this file
*******************************************************************************/
#define SWITCH_PRESSED 0
#define DEV_CONFIG_REG_BASE_ADDRESS     0x9FC02FF0
#define DEV_CONFIG_REG_END_ADDRESS      0x9FC02FFF

#define DATA_RECORD 		0
#define END_OF_FILE_RECORD 	1
#define EXT_SEG_ADRS_RECORD 2
#define EXT_LIN_ADRS_RECORD 4

#define FILEFOUND 0
#define FILENOTFOUND -1

#define POWEROFF 0x01
#define ERASEFILE 0x02

#define ENDOFUSERFLASH 0xE000
#define STARTUSERFLASH 0x100
#define STEPERASE 0x40

#define REC_FLASHED 0
#define REC_NOT_FOUND 1
#define REC_FOUND_BUT_NOT_FLASHED 2


#define MaxSizeBufferWrite MX25L_MaxSizeBufferWrite

#define KC		0x01
#define KUP		0x02
#define KSTARTSTOP	0x04
#define KDOWN	0x08
#define KMENU	0x10
#define KV		0x20
#define KR		0x40
#define KNOKEY  0x0
#define ALLKEYS 0x7f

//preambolo UART
#define PRE1    0x05
#define PRE2    0xA0

//***** StCom ************
#define WAIT		 0		// Stato di attesa comandi
#define WAIT1	 1		// Stato di attesa comandi
#define WAIT2	 2		// Stato di attesa comandi
#define	OUTPUT		 5		// Comando x led
#define	TASTI		10		// Lettura Tasti
#define	ENCODER		15		// Lettura Encoder
#define	CLEARLCD	20		// Cancella Display
#define	PRINTLCD	25		// Stampa a display
#define	PRINTSTAMP	30		// Stampa con la stampante
#define	STATUSSTAMP	33		// status stampante
#define	RESET   	35		// reset
#define SETCHAR		40		// set caratteri display
#define	PRINTFLASHLCD	45	// Stampa a display
#define SETCURS		50		// posiziona cursore
#define STATO		60		// invia stato e versione software
#define READCLOCK   70      
#define WRITECLOCK  80
#define READFLASH   90
#define USBONOFF    100
//#define USBOFF      110
#define SEARCHWOW  120
#define OPENFILE   130
#define READFILE   135
#define APPENDFILE  140
#define COPYFILE  145
#define WAITCOPYFILE  146
#define BOOTL      150


volatile byte stencoder=0;
volatile signed char contencoder=0;
volatile signed char encoderValue=0;
volatile byte buzcount=0;
volatile word ntcIntB=0;
volatile byte lcdval=0;
volatile byte rtprot;
/************************************
type defs
 **************************************/
typedef struct {
    UINT8 *start;
    UINT8 len;
    UINT8 status;
} T_REC;

typedef struct {
    UINT8 RecDataLen;
    DWORD_VAL Address;
    UINT8 RecType;
    UINT8* Data;
    UINT8 CheckSum;
    DWORD_VAL ExtSegAddress;
    DWORD_VAL ExtLinAddress;
} T_HEX_RECORD;	

#ifdef __cplusplus
extern "C" {
#endif
void ConverAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void EraseFlash(void);
int WriteHexRecord2Flash(UINT8* HexRecord);
unsigned char CopyBinToFlash(const char * FileName, unsigned long AddFlashStart, unsigned char flagstart);
unsigned char CopyFlashToMicro(unsigned long AddFlashStart, unsigned long lenght);
unsigned char checkFF(unsigned char * buf,size_t n);
void SetLed(byte output);

void GestCom(void);
#ifdef __cplpusplus
}
#endif

/******************************************************************************
Global Variables
*******************************************************************************/
UINT pointer = 0;
UINT readBytes;

UINT8 asciiBuffer[1024];
UINT8 asciiRec[200];
UINT8 hexRec[100];
byte ComBuf[600];	//buffer per ricezione dati dalla seriale

FSFILE * g_sFileObject;

T_REC record;
unsigned char BufferForCopyBin[MX25L_MaxSizeBufferWrite];

byte tstbuf[4];
byte tstidx;
volatile byte tst;
//byte gUsbStatus  = ST_USB_NO_PRESENT;

/****************************************************************************
Function prototypes
*****************************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void InitializeBoard(void);
BOOL CheckTrigger(void);
void JumpToApp(void);
BOOL ValidAppPresent(void);
BOOL appBpresente;



/*
 * @brief CopyBinToFlash: copia il file "FileName" sulla Flash all'indirizzo "AddFlashStart"
 */
unsigned char CopyBinToFlash(const char * FileName, unsigned long AddFlashStart,byte flagstart){
    static unsigned long AddFlash;
    static size_t n,size,rval;
    static unsigned short w,i;
    static word min, max, val;
    static byte status=0;
    
    if(flagstart)status=0;
    switch (status){
        case 0:
            g_sFileObject=FSfopen( FileName, FS_READ);
            size=1;
            n=MaxSizeBufferWrite;
            min=AddFlashStart>>8;
            max=(g_sFileObject->size + AddFlashStart)>>8;
            if(g_sFileObject != NULL)status=10;else status=255;
            break;
        case 10:
            AddFlash = AddFlashStart;
            status=20;
            break;
        case 20:
            for(;;){
                if (AddFlash % SFMX25L_32_BlockSize == 0) {
                    (void)SFMX25L_16_32_64_BlockErase(AddFlash, MEM1);
                    val=AddFlash>>8;
                    bar(RIGA4,min,max,val,NOPERC);
                }
                rval=FSfread(BufferForCopyBin, size,n, g_sFileObject);
                if(rval==n){
                    if(checkFF(BufferForCopyBin,rval)){
                        if(SFLASH_WrData(AddFlash, BufferForCopyBin, MaxSizeBufferWrite)==0){
                            FSfclose(g_sFileObject);
                            status=255;
                            break;
                        }
                    }
                    AddFlash += MaxSizeBufferWrite;
                    if(AddFlash >= g_sFileObject->size + AddFlashStart){
                        status=254;
                        break;
                    }
                    if (AddFlash % SFMX25L_32_BlockSize == 0)break;
                }else{
                    if(rval!=0){
                        if(checkFF(BufferForCopyBin,rval)){
                            if(SFLASH_WrData(AddFlash, BufferForCopyBin, rval)==0){
                                status=255;
                                break;
                            }
                        }
                    }
                    status=254;
                    break;
                }
            }
            break;
        case 254:
        case 255:
            if(g_sFileObject != NULL){
                FSfclose(g_sFileObject);
                g_sFileObject = NULL;
            }
            break;
    }
    return status;
}


/********************************************************************
 * Function: 	main()
 * Overview: 	Main entry function. If there is a trigger or
 *				if there is no valid application, the device
 *				stays in firmware upgrade mode.
 ********************************************************************/
int main(void) {
    int value;

    // Setup configuration
    DDPCONbits.JTAGEN = 0;
    (void) SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    value = SYSTEMConfigWaitStatesAndPB(GetSystemClock());
    // Enable the cache for the best performance
    CheKseg0CacheOn();
    value = OSCCON;
    while (!(value & 0x00000020)) {
        value = OSCCON; // Wait for PLL lock to stabilize
    }
    INTEnableSystemMultiVectoredInt();

    //******** inizializza gpio *******************************************************
    initGPIO();
    //******** inizializza lcd *******************************************************
    PORTClearBits(REFOUT_PORTID, REFOUT_PIN);
    
    Timer1Init();
    INTEnableInterrupts(); 
    
    initlcd();
    clrlcd();
    setSymbolLCD();
    //******** inizializza flash *******************************************************
    while(!initFlash()){
        //readTst();
        SysDelayUs(10000);  
    }
    //******** inizializza uart *******************************************************
    initUart1();                                //configura uart 485
    initUart2();
    initUart3();                                //configura uart stampante
    appBpresente=ValidAppPresent();
   
    USBPWR_ON();
    USBInitialize(0);
    
    for(;;) GestCom();
    
    return 0;
}

FSFILE * g_sFileObject;
//FSFILE * tempg_sFileObject;
//**********************************************
//******** Gestione Seriale  *******************
//**********************************************
void GestCom(void){
	byte rx=0;
	byte len=0;
    byte lcdadd;
    word w;
    static SearchRec rec;
    static byte stcom=WAIT;
    byte i,j,mmsb,msb,lsb,llsb,ncharmsb,ncharlsb;
    word ncharw;
    dword add;
    static byte checksum;
    char tempencoder;
    static byte fusb=0;
    static byte usbst=0;
    byte buf[512];
    word tempntc;
    size_t rval;
    int res;
//    if(tempstcom!=stcom){
//        txCharUart3(stcom);
//        txCharUart3(tempstcom);
//        txCharUart3('*');
//        printwlcd(RIGA3,stcom,5,0);
//        printwlcd(RIGA4,tempstcom,5,0);
//        tempstcom=stcom;

//    }
    
#ifdef DEBUG_MODE
//    if(g_sFileObject!=tempg_sFileObject){
//        UART3PutHexDWord( (unsigned long) g_sFileObject);
//        tempg_sFileObject=g_sFileObject;
//    }
//    if(tempusbst!=usbst){
//        tempusbst=usbst;
//        sprintf(gdgb, "->usbst %d<-",usbst);txStringUart3((unsigned char *)gdgb);
//    }
//    fusb=1;
#endif    
    if(fusb){
        USBTasks();
        switch(usbst){
            case 0:
                if (USBHostMSDSCSIMediaDetect()) usbst = 10;
                break;
            case 10:
                if (FSInit())usbst = 20;
                break;
            case 20:
                if (!USBHostMSDSCSIMediaDetect()) usbst = 0;
                break;
        }
    }

//    if(prevencoder!=tempencoder){
//        prevencoder=tempencoder;
//        txCharUart3('E');
//        txCharUart3(prevencoder);
//    }
    
	switch(stcom){
		case WAIT:
			(void)RdRS232(0,&rx);
            if(rx==PRE1){
                stcom=WAIT1;
            }else{
//                txCharUart3('(');
//                txCharUart3(rx);
//                txCharUart3(')');
                UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
                UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
            }
            break;
		case WAIT1:
			if(!RdRS232(50,&rx))stcom=WAIT;
            else if(rx==PRE2)stcom=WAIT2;
            else if(rx==PRE1){
                
            }else {
//                txCharUart3('[');
//                txCharUart3(rx);
//                txCharUart3(']');
                UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
                UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
                stcom=WAIT;
            }
            break;
		case WAIT2:
			if(!RdRS232(50,&rx))stcom=WAIT;
            else{
                checksum=rx;
                if(rx=='A')stcom=OPENFILE;
                else if(rx=='a')stcom=READFILE;
                else if(rx=='B')stcom=APPENDFILE;
                else if(rx=='C')stcom=CLEARLCD;
                else if(rx=='c')stcom=COPYFILE;
                else if(rx=='D')stcom=SEARCHWOW;
                else if(rx=='d')stcom=WAITCOPYFILE;
                else if(rx=='F')stcom=PRINTFLASHLCD;
                //else if(rx=='H')stcom=SETCHAR;
                else if(rx=='M')stcom=READFLASH;
                else if(rx=='u')stcom=USBONOFF;
                //else if(rx=='O')stcom=OUTPUT;
                else if(rx=='P')stcom=PRINTLCD;
                //else if(rx=='Q')stcom=USBOFF;            
    			else if(rx=='R')stcom=RESET;
                else if(rx=='S')stcom=PRINTSTAMP;
                else if(rx=='s')stcom=STATUSSTAMP;
                else if(rx=='T')stcom=TASTI;
                else if(rx=='U')stcom=SETCURS;
                //else if(rx=='V')stcom=STATO;
                else if(rx=='w')stcom=WRITECLOCK;
                else if(rx=='W')stcom=READCLOCK;
                else if(rx=='X')stcom=BOOTL;
                else{
//                    txCharUart3('<');
//                    txCharUart3(rx);
//                    txCharUart3('>');
                    UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
                    UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
                }
            }
			break;
		case TASTI:		//Invia stato dei tasti
			ReadSer(3);
            //j=RdRS232();
            if(!RdRS232(50,&rx))stcom=WAIT;
            else{
                checksum='T'^ComBuf[0]^ComBuf[1]^ComBuf[2];
                if(checksum==rx){
                    INTDisableInterrupts();
                    tempencoder = encoderValue; //contencoder;
                    encoderValue = 0; //contencoder=0;
                    tempntc = ntcIntB;
                    INTEnableInterrupts();

                    buzcount=ComBuf[0];
                    SetLed(ComBuf[1]);
                    lcdval=ComBuf[2];
                    set485tx();
                    txCharUart1(tst);
                    txCharUart1(tempencoder);
                    msb=tempntc>>8;
                    lsb=tempntc&0xff;
                    txCharUart1(msb);
                    txCharUart1(lsb);
                    checksum=(tst^tempencoder^msb^lsb);
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();
                }
			stcom=WAIT;	
            }
			break;
		case CLEARLCD:	//Cancella Display
            if(RdRS232(50,&rx)){	//	
                checksum='C';	
                if(rx==checksum){
        			clrlcd();
                	set485tx();
                    i='C';
                    txCharUart1(checksum/*'C'*/);		//comando eseguito
                    txCharUart1(~checksum);
                    set485rx();
                }
            }
			stcom=WAIT;	
			break;
		case PRINTLCD: 	//Stampa a display - attende indirizzo e al max 20 caratteri
            if(RdRS232(50,&lcdadd)){
                if(RdRS232(50,&len)){
                    j=ReadSer(len);
                    if(j==len){
                        if(RdRS232(50,&rx)){
                            checksum='P'^len^lcdadd;
                            for(i=0;i<len;i++)checksum^=ComBuf[i];
                            if(checksum==rx){
                                ComBuf[len]=0;
                                printlcd(lcdadd,(char*)ComBuf);			
                                set485tx();
                                txCharUart1(checksum);		//comando eseguito
                                txCharUart1(~checksum);		//comando eseguito
                                set485rx();
                            }
                        }
                    }
                }
            }
			stcom=WAIT;	
			break;		
		case PRINTFLASHLCD: 	//Stampa a display - 
                stcom=WAIT;	
			break;	
        case RESET:
            ReadSer(5);
            checksum='R'^ComBuf[0]^ComBuf[1]^ComBuf[2]^ComBuf[3];
            if(ComBuf[0]=='E'){
                if(ComBuf[1]=='S'){
                    if(ComBuf[2]=='E'){
                        if(ComBuf[3]=='T'){
                            if(ComBuf[4]==checksum){            
                    			clrlcd();
                    			set485tx();
                                txCharUart1(checksum);		//comando eseguito
                                txCharUart1(~checksum);
                                set485rx();
                                INTDisableInterrupts();
                                for(;;)SoftReset();
                            }
                        }
                    }
                }
            }
            break;
		case PRINTSTAMP: //Invia alla stampante comandi e caratteri	
            
//			j=ReadSer(1);				//legge lunghezza comando
//			len=ComBuf[0];
//			ReadSer(len);			//legge altri byte del comando
//            checksum=len^'S';
//            if(!RdRS232(50,&rx))stcom=WAIT;
//            else{
//                for(i=0;i<len;i++)checksum^=ComBuf[i];
//                if(checksum==rx){
//                    setCTS();
//                    SysDelayUs(10000);
//                    for(i=0;i<len;i++){
//                        txCharUart2(ComBuf[i]);
//                    }
//                    set485tx();
//                    txCharUart1(checksum);			//comando eseguito
//                    txCharUart1(~checksum);
//                    set485rx();
//                }
//            }
            stcom=WAIT;		
			break;
		case STATUSSTAMP:               //Invia alla stampante comando con risposta
//            if(RdRS232(50,&rx)){        //	
//                checksum='s';	
//                if(rx==checksum){
//                    setCTS();
//                    i=readRTS();
//                    txCharUart2(0x1b);
//                    txCharUart2('S');
//                    if(RdUart2(10,&j)==0)i|=0x02;
//        			checksum=i;
//                    set485tx();
//                    txCharUart1(i);
//                    txCharUart1(checksum);		//comando eseguito
//                    txCharUart1(~checksum);		//comando eseguito
//                    set485rx();
//                }
//            }
            stcom=WAIT;	
			break;
//		case SETCHAR:	//Set caratteri speciali del display (rieceve i 64 byte insieme)
//			ReadSer(64);
////			setSymbolLCD((char*)ComBuf);
//			txCharUart1('H');		//comando eseguito
//			stcom=WAIT;
//			break;
		case SETCURS:	//Set cursore lampeggiante sullo schermo
			ReadSer(2);
            if(!RdRS232(50,&rx))stcom=WAIT;
            else{
                checksum='U'^ComBuf[0]^ComBuf[1];
                if(checksum==rx){
                    if(ComBuf[1]==0x00)setCursor(ComBuf[0],DISPON);	else setCursor(ComBuf[0],BLINKON|DISPON);
                    set485tx();
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();
                }
                stcom=WAIT;
            }
			break;
		
//		case STATO:		//Invia la versione software
//            set485tx();
//			ComBuf[0]='0';	//  versione software		
//			ComBuf[1]='2';    //	
//			ComBuf[2]='2';	//	giorno	
//			ComBuf[3]='1';	//	
//			ComBuf[4]='0';	//	mese 
//			ComBuf[5]='8';	//	
//			ComBuf[6]='1';	//	anno
//			ComBuf[7]='7';	//	
//
//			WriteSer(8);
//		//	WrRS232('V');		//comando eseguito
//            set485rx();
//			stcom=WAIT;
//			break;
        case READCLOCK:
//            if(RdRS232(50,&rx)){        //	
//                checksum='W';	
//                if(rx==checksum){
//                    MCP7941x_ReadClock(&RTC);
//                    set485tx();
//                    ComBuf[0]=RTC.RTC_Mday;	//  versione software		
//                    ComBuf[1]=RTC.RTC_Mon;    //	
//                    ComBuf[2]=RTC.RTC_Year;	//	giorno	
//                    ComBuf[3]=RTC.RTC_Hour;	//	
//                    ComBuf[4]=RTC.RTC_Min;	//	mese 
//                    ComBuf[5]=RTC.RTC_Sec;	//	
//                    ComBuf[6]=RTC.RTC_WDay;	//	anno
//                
//                    checksum=ComBuf[0]^ComBuf[1]^ComBuf[2]^ComBuf[3]^ComBuf[4]^ComBuf[5]^ComBuf[6];
//                    WriteSer(7);
//                    txCharUart1(checksum);		//comando eseguito
//                    txCharUart1(~checksum);		//comando eseguito
//                    set485rx();
//                }
//            }
            stcom=WAIT;	
            break;
        case WRITECLOCK:
//                if(RdRS232(50,&RTC.RTC_Mday)){	//  versione software		
//                    if(RdRS232(50,&RTC.RTC_Mon)){    //	
//                        if(RdRS232(50,&RTC.RTC_Year)){
//                            if(RdRS232(50,&RTC.RTC_Hour)){
//                                if(RdRS232(50,&RTC.RTC_Min)){
//                                    if(RdRS232(50,&RTC.RTC_Sec)){
//                                        if(RdRS232(50,&RTC.RTC_WDay)){
//                                            if(RdRS232(50,&rx)){
//                                                checksum='w'^RTC.RTC_Mday^RTC.RTC_Mon^RTC.RTC_Year^RTC.RTC_Hour^RTC.RTC_Min^RTC.RTC_Sec^RTC.RTC_WDay;	
//                                                if(rx==checksum){
//                                                    set485tx();
//                                                    txCharUart1(checksum);		//comando eseguito
//                                                    txCharUart1(~checksum);		//comando eseguito
//                                                    set485rx();
//                                                    (void)MCP7941x_WriteClock(&RTC, 0x80);
//                                                    (void) MCP7941x_StartClock();
//                                                    (void) MCP7941x_ConnectVbat();
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
                stcom=WAIT;
            break;
        case READFLASH:
                if(RdRS232(50,&mmsb)){
                    if(RdRS232(50,&msb)){    //	
                        if(RdRS232(50,&lsb)){
                            if(RdRS232(50,&llsb)){	//	
                                if(RdRS232(50,&ncharmsb)){	//	
                                    if(RdRS232(50,&ncharlsb)){	//	
                                        if(RdRS232(50,&rx)){	//	
                                            checksum='M'^mmsb^msb^lsb^llsb^ncharmsb^ncharlsb;	
                                            if(rx==checksum){
                                                add=mmsb;
                                                add=add<<8;
                                                add|=msb;
                                                add=add<<8;
                                                add|=lsb;
                                                add=add<<8;
                                                add|=llsb;
                                                ncharw=ncharmsb;
                                                ncharw=ncharw<<8;
                                                ncharw|=ncharlsb;
                                                SFLASH_RdData(add , buf, ncharw);
                                                set485tx();
                                                checksum=0;
                                                for(w=0;w<ncharw;w++){
                                                    txCharUart1(buf[w]);
                                                    checksum^=buf[w];
                                                }
                                                txCharUart1(checksum);		//comando eseguito
                                                txCharUart1(~checksum);		//comando eseguito
                                                set485rx();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            stcom=WAIT;
            break;
        case USBONOFF:
            ReadSer(6);
            checksum='u'^ComBuf[0]^ComBuf[1]^ComBuf[2]^ComBuf[3]^ComBuf[4];
            if(ComBuf[0]=='s'){
                if(ComBuf[1]=='b'){
                    if(ComBuf[2]=='O'){
                        if(ComBuf[3]=='N'){
                            if(ComBuf[4]==' '){ //usbON
                                if(ComBuf[5]==checksum){
                                    USBPWR_ON();
                                    USBInitialize(0);
                                    fusb=1;
                                    usbst=0;
                                    set485tx();
                                    txCharUart1(checksum);		//comando eseguito
                                    txCharUart1(~checksum);		//comando eseguito
                                    set485rx();
                                    
                                }
                            }
                        }else if(ComBuf[3]=='F'){//usbOFF
                            if(ComBuf[4]=='F'){
                                if(ComBuf[5]==checksum){
                                    USBHostShutdown();
                                    USBPWR_OFF();
                                    fusb=0;
                                    usbst=0;
                                    set485tx();
                                    txCharUart1(checksum);		//comando eseguito
                                    txCharUart1(~checksum);		//comando eseguito
                                    set485rx();
                                    
                                }
                            }
                        }
                    }else if(ComBuf[2]=='S'){
                        if(ComBuf[3]=='T'){//usbST
                            if(ComBuf[4]==' '){
                                if(ComBuf[5]==checksum){
                                    checksum=usbst;
                                    set485tx();
                                    txCharUart1(usbst);
                                    txCharUart1(checksum);		//comando eseguito
                                    txCharUart1(~checksum);		//comando eseguito
                                    set485rx();
                                }
                            }
                        }
                    }else if(ComBuf[2]=='C'){
                        if(ComBuf[3]=='F'){//close file usbCF
                            if(ComBuf[4]==' '){
                                if(ComBuf[5]==checksum){
                                    checksum='D';
                                    if((g_sFileObject!=NULL)&&(usbst==20)){
                                        res=FSfclose(g_sFileObject);
                                        if (res==0)checksum='D';else checksum='d';
                                    }else checksum='d'; 
                                    g_sFileObject=NULL;
                                    set485tx();
                                    txCharUart1(checksum);
                                    txCharUart1(checksum);		//comando eseguito
                                    txCharUart1(~checksum);		//comando eseguito
                                    set485rx();
                                }
                            }
                        }
                    }
                }
            }
            stcom=WAIT;
            break;
        case SEARCHWOW:
            if(RdRS232(50,&rx)){	//	
                checksum='D';	
                if(rx==checksum){
                    if (FindFirst("*.wow", ATTR_ARCHIVE, &rec) == FILEFOUND) {
 //                       gFileStatus = ST_FILE_FOUND;
 //                       memcpy(&ComBuf[0],&rec.filename[0],12);
                    } else{ 
//                        gFileStatus = ST_FILE_NOT_FOUND;
                        rec.filename[0]=0xff;
                    }
                    set485tx();
                    checksum=0;
                    for(i=0;i<12;i++){
                        checksum^=rec.filename[i];
                        txCharUart1(rec.filename[i]);
                    }
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();

                }
            }
            stcom=WAIT;
            break;
        case OPENFILE:
//            j=ReadSer(12);
//            if(RdRS232(50,&rx)){
//                checksum='A';
//                for(i=0;i<12;i++)checksum^=ComBuf[i];
//                if(checksum==rx){
//                    ComBuf[12]=0;
//                    //(void)FSremove (ComBuf);
//                    (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
//                    i='a';
//                    j=0;
//                    if(usbst==20){
//                        g_sFileObject = FSfopen((const char *)ComBuf, FS_WRITEPLUS);
//                        if(g_sFileObject!=NULL)i='A';else i='a';
//                    }
//                    j=FSerror();
//                    checksum=i^j;
//                    set485tx();
//                    txCharUart1(i);
//                    txCharUart1(j);
//                    txCharUart1(checksum);		//comando eseguito
//                    txCharUart1(~checksum);		//comando eseguito
//                    set485rx();
//                }
//            }
            stcom=WAIT;
            break;
        case READFILE:
            j=ReadSer(12);
            if(RdRS232(50,&rx)){
                checksum='a';
                for(i=0;i<12;i++)checksum^=ComBuf[i];
                if(checksum==rx){
                    ComBuf[12]=0;
                    //(void)FSremove (ComBuf);
//                    (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
                    i='a';
                    j=0;
                    if(usbst==20){
                        g_sFileObject = FSfopen((const char *)ComBuf, FS_READ);
                        if(g_sFileObject!=NULL)i='A';else i='a';
                    }
                    j=FSerror();
                    checksum=i^j;
                    set485tx();
                    txCharUart1(i);
                    txCharUart1(j);
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();
                }
            }
            stcom=WAIT;
            break;
        case COPYFILE:
            j=ReadSer(12);
            if(RdRS232(50,&rx)){
                checksum='c';
                for(i=0;i<12;i++)checksum^=ComBuf[i];
                if(checksum==rx){
                    checksum=CopyBinToFlash(ComBuf, 0,1) ;
                    set485tx();
                    txCharUart1(checksum);
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();
                }
            }
            stcom=WAIT;
            break;
        case WAITCOPYFILE:
            if(RdRS232(50,&rx)){
                checksum='d';
                if(checksum==rx){
                    checksum=CopyBinToFlash(ComBuf, 0,0) ;
                    set485tx();
                    txCharUart1(checksum);
                    txCharUart1(checksum);		//comando eseguito
                    txCharUart1(~checksum);		//comando eseguito
                    set485rx();
                }
            }
            stcom=WAIT;
            break;
        case APPENDFILE:
//            j=ReadSer(1);				//legge lunghezza comando
//			len=ComBuf[0];
//			ReadSer(len);
//            if(RdRS232(50,&rx)){
//                checksum=len^'B';
//                for(i=0;i<len;i++)checksum^=ComBuf[i];
//                if(checksum==rx){
//                    checksum='B';
//                    i='B';
//                    j=0;
//                    if((g_sFileObject!=NULL)&&(usbst==20)){
//                        rval = FSfwrite(ComBuf, 1, len, g_sFileObject);
//                        if (rval == len)checksum='B';else checksum='b';
//                    }else checksum='b';
//                    j=FSerror();
//                    checksum=i^j;
//                    set485tx();
//                    txCharUart1(i);
//                    txCharUart1(j);
//                    txCharUart1(checksum);		//comando eseguito
//                    txCharUart1(~checksum);		//comando eseguito
//                    set485rx();
//                }
//            }
            stcom=WAIT;
            break;
        case BOOTL:
            ReadSer(4);
            checksum='X'^ComBuf[0]^ComBuf[1]^ComBuf[2];
            if(ComBuf[0]=='C'){
                if(ComBuf[1]=='A'){
                    if(ComBuf[2]=='V'){ //bootloader Check App Valid
                        if(ComBuf[3]==checksum){
                            checksum=appBpresente;
                            set485tx();
                            txCharUart1(appBpresente);
                            txCharUart1(checksum);		//comando eseguito
                            txCharUart1(~checksum);		//comando eseguito
                            set485rx();
                        }
                    }
                }
            }else if(ComBuf[0]=='P'){
                if(ComBuf[1]=='S'){
                    if(ComBuf[2]=='B'){ //bootloader Programma SCHEDA B
                        if(ComBuf[3]==checksum){
                            CopyFlashToMicro(ADD_FILE_FWB, ADDFLASHEND);
                            set485tx();
                            txCharUart1(checksum);		//comando eseguito
                            txCharUart1(~checksum);		//comando eseguito
                            set485rx();
                        }
                    }
                }
            }else if(ComBuf[0]=='J'){
                if(ComBuf[1]=='S'){
                    if(ComBuf[2]=='B'){ //bootloader Jump App SCHEDA B
                        if(ComBuf[3]==checksum){
                            set485tx();
                            txCharUart1(checksum);		//comando eseguito
                            txCharUart1(~checksum);		//comando eseguito
                            set485rx();
                            

                            USBHostShutdown();
                            USBPWR_OFF();
//                            for(w=0;w<30000;w++)USBTasks();
                            rtprot=100;
                            while(rtprot);
                            USBDisableInterrupts();
                            (void) INTDisableInterrupts();
                            (void) DmaSuspend();
                            
                            JumpToApp();
                            
                            
//                            U1CON = 0;\
//                            U1IE = 0;
//                            U1OTGIE = 0;
//                            U1PWRCbits.USBPWR = 1;
//
//                            
//                            INTDisableInterrupts();
//                            //USBDisableInterrupts();
//                            JumpToApp();
                        }
                    }
                }
            }else if(ComBuf[0]=='B'){
                if(ComBuf[1]=='L'){
                    if(ComBuf[2]=='M'){ //bootloader Mode
                        if(ComBuf[3]==checksum){
                            checksum=1;
                            set485tx();
                            txCharUart1(1);
                            txCharUart1(checksum);		//comando eseguito
                            txCharUart1(~checksum);		//comando eseguito
                            set485rx();
                        }
                    }
                }
            }
            stcom=WAIT;
            break;
	}
}

/********************************************************************
* Function: 	JumpToApp()
* Overview: 	Jumps to application.
********************************************************************/
void JumpToApp(void){	
	void (*fptr)(void);
	fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
	fptr();
}	

/********************************************************************
* Function: 	ConvertAsciiToHex()
* Input: 		Ascii buffer and hex buffer.
* Overview: 	Converts ASCII to Hex.
********************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec){
	UINT8 i = 0;
	UINT8 k = 0;
	UINT8 hex;
    
	while((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66)){
		// Check if the ascci values are in alpha numeric range.		
		if(asciiRec[i] < 0x3A){			// Numerical reperesentation in ASCII found.
			hex = asciiRec[i] & 0x0F;
		}else{              			// Alphabetical value.
			hex = 0x09 + (asciiRec[i] & 0x0F);						
		}
		// Following logic converts 2 bytes of ASCII to 1 byte of hex.
		k = i%2;
		if(k){
			hexRec[i/2] |= hex;
		}else{
			hexRec[i/2] = (hex << 4) & 0xF0;
		}	
		i++;		
	}			
}
// Do not change this
//#define FLASH_PAGE_SIZE 0x1000
#define FLASH_PAGE_SIZE 0x400 // 1K
/********************************************************************
* Function: 	EraseFlash()
* Overview: 	Erases Flash (Block Erase).
********************************************************************/
void EraseFlash(void){
	void * pFlash;
    UINT result;
    INT i;

    pFlash = (void*)APP_FLASH_BASE_ADDRESS;									
    for( i = 0; i < ((APP_FLASH_END_ADDRESS - APP_FLASH_BASE_ADDRESS + 1)/FLASH_PAGE_SIZE); i++ ){
        result = NVMemErasePage( pFlash + (i*FLASH_PAGE_SIZE) );
        // Assert on NV error. This must be caught during debug phase.

        if(result != 0){
            // We have a problem. This must be caught during the debug phase.
            printlcd(RIGA1,"ERROR Erasing  ");
            printlcd(RIGA2,"Memory         ");
            while(1);
        } 
    }			           	     
}

/********************************************************************
* Function: 	WriteHexRecord2Flash()
* Overview: 	Writes Hex Records to Flash.
********************************************************************/
int WriteHexRecord2Flash(UINT8* HexRecord){
	static T_HEX_RECORD HexRecordSt;
	UINT8 Checksum = 0;
	UINT8 i;
	UINT WrData;
	UINT RdData;
	void* ProgAddress;
	UINT result;
    
	HexRecordSt.RecDataLen = HexRecord[0];
	HexRecordSt.RecType = HexRecord[3];	
	HexRecordSt.Data = &HexRecord[4];	
	
	// Hex Record checksum check.
	for(i = 0; i < HexRecordSt.RecDataLen + 5; i++){
		Checksum += HexRecord[i];
	}	
	
    if(Checksum != 0){
	    //Error. Hex record Checksum mismatch.
	    //Indicate Error by switching ON all LEDs.
	    //Error();
	    // Do not proceed further.
	    //while(1);
        return 2;
	} 
	else{
		// Hex record checksum OK.
		switch(HexRecordSt.RecType){
			case DATA_RECORD:  //Record Type 00, data record.
				HexRecordSt.Address.byte.MB = 0;
                HexRecordSt.Address.byte.UB = 0;
                HexRecordSt.Address.byte.HB = HexRecord[1];
                HexRecordSt.Address.byte.LB = HexRecord[2];
                
                // Derive the address.
                HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;
                
                while(HexRecordSt.RecDataLen) // Loop till all bytes are done.
                {											
                    // Convert the Physical address to Virtual address. 
                    ProgAddress = (void *)PA_TO_KVA0(HexRecordSt.Address.Val);
                    
                    // Make sure we are not writing boot area and device configuration bits.
                    if(((ProgAddress >= (void *)APP_FLASH_BASE_ADDRESS) && (ProgAddress <= (void *)APP_FLASH_END_ADDRESS))
                            && ((ProgAddress < (void*)DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > (void*)DEV_CONFIG_REG_END_ADDRESS)))
                    {
                        if(HexRecordSt.RecDataLen < 4){								
                            // Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
                            // we don't write junk data in such cases.
                            WrData = 0xFFFFFFFF;
                            memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);	
                        }
                        else{	
                            memcpy(&WrData, HexRecordSt.Data, 4);
                        }		
                        // Write the data into flash.	
                        result = NVMemWriteWord(ProgAddress, WrData);	
                        // Assert on error. This must be caught during debug phase.		
                        if(result != 0){
                            return 2;
                        }									
                    }	
                    
                    // Increment the address.
                    HexRecordSt.Address.Val += 4;
                    // Increment the data pointer.
                    HexRecordSt.Data += 4;
                    // Decrement data len.
                    if(HexRecordSt.RecDataLen > 3){
                        HexRecordSt.RecDataLen -= 4;
                    }	
                    else{
                        HexRecordSt.RecDataLen = 0;
                    }	
                }
                break;
                
			case EXT_SEG_ADRS_RECORD:  // Record Type 02, defines 4th to 19th bits of the data address.
			    HexRecordSt.ExtSegAddress.byte.MB = 0;
				HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
				HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
				HexRecordSt.ExtSegAddress.byte.LB = 0;
				// Reset linear address.
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
				
			case EXT_LIN_ADRS_RECORD:   // Record Type 04, defines 16th to 31st bits of the data address. 
				HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
				HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
				HexRecordSt.ExtLinAddress.byte.HB = 0;
				HexRecordSt.ExtLinAddress.byte.LB = 0;
				// Reset segment address.
				HexRecordSt.ExtSegAddress.Val = 0;
				break;
				
			case END_OF_FILE_RECORD:  //Record Type 01, defines the end of file record.
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				// Disable any interrupts here before jumping to the application.
				//USBDisableInterrupts();
                //SoftReset();
				//JumpToApp();
                return 1;
				break;
				
			default: 
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
		}		
	}	
    return 0;
}	

/********************************************************************
* Function: 	ValidAppPresent()
* Output:		TRUE: If application is valid.
* Overview: 	Logic: Check application vector has 
    some value other than "0xFFFFFF"
********************************************************************/
BOOL ValidAppPresent(void){
	volatile UINT32 *AppPtr;
	
	AppPtr = (UINT32*)USER_APP_RESET_ADDRESS;

	if(*AppPtr == 0xFFFFFFFF){
		return FALSE;
	}
	else{
		return TRUE;
	}
}			

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )
 
  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.
 
  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.
 
  Precondition:
    None
 
  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data
 
  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled
 
  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/
BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size ){
    switch( event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            //deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }

    return FALSE;
}


/*
 * @brief CopyFlashToMicro: copia un file di applicazione dalla Flash esterna alla Program Flash
 */
unsigned char CopyFlashToMicro(unsigned long AddFlashStart, unsigned long lenght){
    UINT8 lasciiBuffer[1024];
    int i;
    T_REC lrecord;
    UINT lpointer = 0;
    unsigned long AddFlash;
    byte isProgramEnd=0;

    EraseFlash();
    lrecord.status = REC_NOT_FOUND;
    
    for (AddFlash = AddFlashStart; AddFlash < lenght + AddFlashStart;){
        readBytes=SFLASH_RdData((dword) AddFlash, &lasciiBuffer[lpointer], 512);
        AddFlash += 512;

        for(i = 0; i < (readBytes + lpointer); i++){
            //This state machine seperates-out the valid hex records from the read 512 bytes.
            switch(lrecord.status)
            {
                case REC_FLASHED:
                case REC_NOT_FOUND:
                    if(lasciiBuffer[i] == ':')
                    {
                        // We have a record found in the 512 bytes of data in the buffer.
                        lrecord.start = &lasciiBuffer[i];
                        lrecord.len = 0;
                        lrecord.status = REC_FOUND_BUT_NOT_FLASHED;
                    }
                    break;
                case REC_FOUND_BUT_NOT_FLASHED:
                    if((lasciiBuffer[i] == 0x0A) || (lasciiBuffer[i] == 0xFF))
                    {
                        // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                        // Start the hex conversion from element
                        // 1. This will discard the ':' which is
                        // the start of the hex record.
                        ConvertAsciiToHex(&lrecord.start[1],hexRec);
                        isProgramEnd=WriteHexRecord2Flash(hexRec);
                        lrecord.status = REC_FLASHED;
                    }
                    break;
            }
            // Move to next byte in the buffer.
            lrecord.len ++;
        }
        if(lrecord.status == REC_FOUND_BUT_NOT_FLASHED){
            // We still have a half read record in the buffer. The next half part of the record is read 
            // when we read 512 bytes of data from the next file read. 
            memcpy(lasciiBuffer, lrecord.start, lrecord.len);
            lpointer = lrecord.len;
            lrecord.status = REC_NOT_FOUND;
        }
        else{
            lpointer = 0;
        }
        
        switch(isProgramEnd){
            case 0:
                //Programmazione ancora in corso
                break;
            case 1:
                //Programmazione conclusa con successo
               // printlcd(RIGA1,"B: Program micro OK ");

                return 1;
                break;
            case 2:
                //Programmazione Errore
                EraseFlash();
                lrecord.status = REC_NOT_FOUND;
                isProgramEnd   = 0;
                AddFlash = AddFlashStart;
                break;
        }
    }
    return 1;
}


unsigned char checkFF(unsigned char * buf,size_t n){
    size_t i;
    for(i=0;i<n;i++)if(buf[i]!=0xff)return 1;
    return 0;
}
        
/*********************End of File************************************/



void __ISR(_TIMER_1_VECTOR, IPL2AUTO) Timer1Handler(void) {
    
    INTClearFlag(INT_T1);   // Clear the interrupt flag
    if(flags1&FIOINT){

        LCDBUS_IN();
        ENTST_LOW();
        tstbuf[tstidx&3]=PORTE;
        ENTST_HIGH();

        tstidx++;					                       
        tst=tst&(~(tstbuf[0]&tstbuf[1]&tstbuf[2]&tstbuf[3]));//filtro tastiera
        tst=tst|(~(tstbuf[0]|tstbuf[1]|tstbuf[2]|tstbuf[3]));
        tst&=0xfe;
    }
    if (rtprot)rtprot--;

}

//************************************************
//******* Set Uscite Led  ************************
//************************************************
void SetLed(byte output){
	
	if(output&LD3)PORTSetPinsDigitalOut(LED1_PORTID,LED1_PIN);else PORTSetPinsDigitalIn(LED1_PORTID,LED1_PIN);//PORTClearBits(LED1_PORTID, LED1_PIN);else PORTSetBits(LED1_PORTID, LED1_PIN);//LV
	if(output&LD1)PORTSetPinsDigitalOut(LED2_PORTID,LED2_PIN);else PORTSetPinsDigitalIn(LED2_PORTID,LED2_PIN);//PORTClearBits(LED2_PORTID, LED2_PIN);else PORTSetBits(LED2_PORTID, LED2_PIN);//LE
	if(output&LD2)PORTSetPinsDigitalOut(LED3_PORTID,LED3_PIN);else PORTSetPinsDigitalIn(LED3_PORTID,LED3_PIN);//PORTClearBits(LED3_PORTID, LED3_PIN);else PORTSetBits(LED3_PORTID, LED3_PIN);
	if(output&LD4)PORTSetPinsDigitalOut(LED4_PORTID,LED4_PIN);else PORTSetPinsDigitalIn(LED4_PORTID,LED4_PIN);//PORTClearBits(LED4_PORTID, LED4_PIN);else PORTSetBits(LED4_PORTID, LED4_PIN);
    if(output&LEDUSB)PORTSetBits(LCDRST_PORTID,LCDRST_PIN);else PORTClearBits(LCDRST_PORTID,LCDRST_PIN);//PORTSetPinsDigitalOut(LCDRST_PORTID,LCDRST_PIN);else PORTSetPinsDigitalIn(LCDRST_PORTID,LCDRST_PIN);//PORTClearBits(LED4_PORTID, LED4_PIN);else PORTSetBits(LED4_PORTID, LED4_PIN);
    if(output&LCDON)PORTSetBits(LCDON_PORTID,LCDON_PIN);else PORTClearBits(LCDON_PORTID,LCDON_PIN);
    
}


static enum {
    EXCEP_IRQ = 0,           /* interrupt */
    EXCEP_AdEL = 4,          /* address error exception (load or ifetch) */
    EXCEP_AdES,              /* address error exception (store) */
    EXCEP_IBE,               /* bus error (ifetch) */
    EXCEP_DBE,               /* bus error (load/store) */
    EXCEP_Sys,               /* syscall */
    EXCEP_Bp,                /* breakpoint */
    EXCEP_RI,                /* reserved instruction */
    EXCEP_CpU,               /* coprocessor unusable */
    EXCEP_Overflow,          /* arithmetic overflow */
    EXCEP_Trap,              /* trap (possible divide by zero) */
    EXCEP_IS1 = 16,          /* implementation specfic 1 */
    EXCEP_CEU,               /* CorExtend Unuseable */
    EXCEP_C2E                /* coprocessor 2 */
} _excep_code;
static unsigned int _epc_code;
static unsigned int _excep_addr;


void _general_exception_handler(void){
    unsigned long t0 = _CP0_GET_COUNT(); /* Used for NVMOP 6 us Delay */

    _excep_code=_CP0_GET_CAUSE() & 0x0000007C >> 2;
    _excep_addr=_CP0_GET_EPC();

    _CP0_SET_STATUS(_CP0_GET_STATUS()&0xFFFFFFE); /* Disable Interrupts */

    while (1){
        txCharUart3(_excep_code);
        LCDBUS_IN();
        ENTST_LOW();
        ENTST_HIGH();
    }
}
