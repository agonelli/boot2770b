/* 
 * File:   HWUART.h
 * Author: andrea
 *
 * Created on 7 giugno 2013, 15.41
 */

#ifndef HWUART_H
#define	HWUART_H

#ifdef	__cplusplus
extern "C" {
#endif

void initUart1(void);
void initUart2(void);
void initUart3(void);
void txCharUart1(unsigned char b);
void txUintUart1(unsigned int i);
unsigned char rxCharUart1(unsigned char * b);
unsigned char dataReadyUart1(void);
unsigned char readCharUart1(void);
unsigned char RdRS232(unsigned char timeout,unsigned char * v);
unsigned char ReadSer(unsigned char len);
void WriteSer(unsigned char  len);
void WriteUart3(unsigned char* buf,unsigned char  len);
unsigned char RdUart2(unsigned char timeout,unsigned char * v);
unsigned char rxCharUart2(unsigned char * b);
//
//void initUart2(void);
//void txCharUart2(unsigned char b);
//void txUintUart2(unsigned int i);
//unsigned char rxCharUart2(unsigned char * b);
//void UART_CAP_Send(unsigned char * Buffer, unsigned short BufferLen);
//void initUart4(void);
//void txCharUart4(unsigned char b);

void set485tx(void);
void set485rx(void);
void initUart3(void);

void txCharUart3(unsigned char b);
unsigned char rxCharUart3(unsigned char * b);
unsigned char dataReadyUart3(void);
unsigned char readCharUart3(void);
void txStringUart3( char * b);
void UART3PutHex( int toPrint );
void UART3PutHexDWord( unsigned long toPrint );
void  UART3PutDec(unsigned char dec);

#ifdef	__cplusplus
}
#endif

#endif	/* HWUART_H */

