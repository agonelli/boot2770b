/* 
 * File:   SPI.h
 * Author: andrea
 *
 * Created on 8 maggio 2013, 8.17
 */

#ifndef SPI_H
#define	SPI_H

#ifdef	__cplusplus
extern "C" {
#endif


#include "GenericTypeDefs.h"

typedef struct
{
    int    channel;
#ifdef __PIC32MX
	int	baudRate;
	int     dummy;
#else
	int     primaryPrescale;
	int     secondaryPrescale;
#endif
	char    cke;
	char    ckp;
	char    smp;
	char    mode;

} DRV_SPI_INIT_DATA;

/* SPI SFR definitions. i represents the SPI
   channel number.
   valid i values are: 1, 2, 3
   for PIC24F & PIC32MX device families
*/
    #define DRV_SPI_STAT(i)      SPI##i##STAT
    #define DRV_SPI_STATbits(i)  SPI##i##STATbits
#ifdef __PIC32MX__
    #define DRV_SPI_CON(i)       SPI##i##CON
    #define DRV_SPI_CONbits(i)   SPI##i##CONbits
    #define DRV_SPI_BRG(i)       SPI##i##BRG
    #define DRV_SPI_BRGbits(i)   SPI##i##BRGbits
#else
    #define DRV_SPI_CON(i)       SPI##i##CON1
    #define DRV_SPI_CONbits(i)   SPI##i##CON1bits
    #define DRV_SPI_CON2(i)      SPI##i##CON2
    #define DRV_SPI_CON2bits(i)  SPI##i##CON2bits
#endif
    #define DRV_SPI_BUF(i)       SPI##i##BUF
    #define DRV_SPI_BUFbits(i)   SPI##i##BUFbits

/* macros that defines the SPI signal polarities */
    #define SPI_CKE_IDLE_ACT     0        // data change is on active clock to idle clock state
    #define SPI_CKE_ACT_IDLE     1        // data change is on idle clock to active clock state

    #define SPI_CKP_ACT_HIGH     0        // clock active state is high level
    #define SPI_CKP_ACT_LOW      1        // clock active state is low level

    #define SPI_SMP_PHASE_MID    0        // master mode data sampled at middle of data output time
    #define SPI_SMP_PHASE_END    1        // master mode data sampled at end of data output time

    #define SPI_MODE_8BITS       0        // communication mode set at 8-bits
    #define SPI_MODE_16BITS      1        // communication mode set at 16-bits
    #define SPI_MODE_32BITS      2        // communication mode set at 32-bits

    #define SPI_MST_MODE_ENABLE  1        // SPI master mode enabled
    #define SPI_MST_MODE_DISABLE 0        // SPI master mode disabled, use SPI in slave mode

    #define SPI_MODULE_ENABLE    1        // Enable SPI
    #define SPI_MODULE_DISABLE   0        // Disable SPI

void DRV_SPI_Initialize(const unsigned int channel, DRV_SPI_INIT_DATA *pData);
BYTE SPIGet (unsigned int channel);
int SPILock(unsigned int channel);
void SPIPut(unsigned int channel, unsigned char data);
void SPIUnLock(unsigned int channel);

#ifdef	__cplusplus
}
#endif

#endif	/* SPI_H */

