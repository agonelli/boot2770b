/* 
 * File:   HW_SPI.h
 * Author: matteob
 *
 * Created on 12 aprile 2013, 11.26
 */

#ifndef HW_SPI_H
#define	HW_SPI_H

#ifdef	__cplusplus
extern "C" {
#endif



unsigned char  initFlash(void);
unsigned int FreadUINT32(unsigned int fadd);
unsigned short FreadUINT16(unsigned int fadd);
unsigned short FreadUINT16u(unsigned int fadd);
void SFMX25L_16_32_64_SelectFlash(unsigned char mem);
void SFMX25L_16_32_64_DeselectFlash(unsigned char mem);
unsigned short SFLASH_RdData(unsigned long addr, unsigned char* data, unsigned short len);
unsigned short SFLASH_WrData(unsigned long addr, unsigned char* data, unsigned short len);
byte SFLASH_BlockErase(dword addr);

//void DRV_SPI_Initialize(unsigned int channel, DRV_SPI_INIT_DATA *pData);


//*************** indirizzi flash ********************

#define STARTLANGADD	0x0000
#define ENDLANGADD	0x50000
#define ADDSCOFILE      ENDLANGADD  //0x50000
#define ENDSCOFILE      (ENDLANGADD+0x10000)
#define STARTDBADD	ENDSCOFILE  //0x60000
#define ENDDBADD	(STARTDBADD+0x20000) //riservati 128K per file db 0x80000

//****mappatura a settori a partire dall'alto verso il basso
#define ADDFilterCodeStart (SFMX25L_32_Size - SFMX25L_32_SectorSize) //0x400000-0x1000=0x3ff000
#define ADDParameterStart (ADDFilterCodeStart - SFMX25L_32_SectorSize)//0x3ff000-0x1000=0x3fe000
#define ADDPersonalDb   (ADDParameterStart - SFMX25L_32_SectorSize)//0x3ff000-0x1000=0x3fd000



#define Channel_For_MX25L 2

#define MX25L_MaxSizeBufferWrite 256

#define MEM1	0x00
#define MEM2	0x01

#define ID_MX25L 0xc215

#ifdef	__cplusplus
}
#endif

#endif	/* HW_SPI_H */

