#include "xc.h"
#include"peripheral\uart.h"

#include "GLOBAL.H"
#include "HWUART.H"
#include <plib.h>
#include "Hardwareprofile.H"
#include "system.h"
#include "HWGPIO.h"
#include "HWTIMER.h"
//#include "protocolUtils.h"

#define bps_UART_485 57600//38400//19200
//#ifdef _DEBUG_
//#define bps_UART_PRN 115200
//#else
//#define bps_UART_PRN 9600
//#endif
#define bps_UART_PRN 9600
extern byte ComBuf[];	//buffer per ricezione dati dalla seriale
extern volatile byte rtprot;
//********************************************************************************************
//***************************** UART1 --> CAP ************************************************
//********************************************************************************************
void initUart1(void){
    RPD3Rbits.RPD3R=0x03;//rpd3
    U1RXRbits.U1RXR=0x00;//rpd2
    UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART1, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART1, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART1, GetPeripheralClock(), bps_UART_485);
    UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    set485rx();

}

void txCharUart1(unsigned char b){
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, b);
}

void txUintUart1(unsigned int i){
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, (unsigned char)(i>>24));
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, (unsigned char)((i&0xff0000)>>16));
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, (unsigned char)((i&0xff00)>>8));
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, (unsigned char)(i&0xff));

}

unsigned char rxCharUart1(unsigned char * b){
    if(UARTReceivedDataIsAvailable ( UART1 )){
        *b=UARTGetDataByte ( UART1);
        return 1;
    }else return 0;
}
unsigned char rxCharUart2(unsigned char * b){
    if(UARTReceivedDataIsAvailable ( UART2 )){
        *b=UARTGetDataByte ( UART2);
        return 1;
    }else return 0;
}

unsigned char dataReadyUart1(void){
    if(UARTReceivedDataIsAvailable ( UART1 ))return 1;else return 0;
}
unsigned char readCharUart1(void){
    return UARTGetDataByte ( UART1);
}

//************************************************
//******* Legge Buffer dalla seriale *************
//************************************************
byte ReadSer(byte len){
	byte i=0;

	if(len==0){
		if(!RdRS232(50,&ComBuf[0]))return 0;
		i=1;
    for(;;){
			if(!RdRS232(50,&ComBuf[i]))return 0;
			if(ComBuf[i]==0x00)break;
			i++;
    }
	}else{
		for(i=0;i<len;i++){
			if(!RdRS232(50,&ComBuf[i]))return 0;
}
	}
    return i;
}

//************************************************
//******** Scrive Buffer nella seriale ***********
//************************************************
void WriteSer(byte  len){
	byte i=0; 	
	
	for(i=0;i<len;i++){
		txCharUart1(ComBuf[i]);
}
}

byte RdRS232(unsigned char timeout,unsigned char * v){
    //byte val;
    byte b;
    UART_LINE_STATUS st;
    rtprot=timeout/10;
    for(;;){
        st= UARTGetLineStatus ( UART1 );        
        if((st&UART_FRAMING_ERROR)||(st&UART_PARITY_ERROR)||(st&UART_OVERRUN_ERROR)){
            UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
            UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
        }        
        if(rxCharUart1(v/*&val*/)){
            //txCharUart3(val);
            return 1;//val;
        }
        if((timeout!=0)&&(rtprot==0))return 0;
    }
}
        


void set485tx(void){
    SysDelayUs(50);
    PORTSetBits(DIR485_PORTID, DIR485_PIN);
}

void set485rx(void){
    while(!UARTTransmitterIsReady(UART1));
    while(!UARTTransmissionHasCompleted ( UART1));
    PORTClearBits(DIR485_PORTID, DIR485_PIN);
}

//void UART_CAP_Send(unsigned char * Buffer, unsigned short BufferLen) {
//    unsigned short i;
////    EnableTX485();
//    //DelayUs(20);
//    for (i = 0; i < BufferLen; i++) {
//        txCharUart1(Buffer[i]);
//        SysDelayUs(10000000 / bps_UART_CAP);
//    }
////    DisableTX485();
//}

////************ UART2 --> gas analyzer *****************************
//void initUart2(void){
//    RPF5Rbits.RPF5R=0x01;//rpf5
//    U2RXRbits.U2RXR=0x02;//rpf4
//    UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
//    UARTSetFifoMode(UART2, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
//    UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
//    UARTSetDataRate(UART2, GetPeripheralClock(), bps_UART_PRN);
//    UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
//
//}
//
//void txCharUart2(unsigned char b){
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, b);
//}
//
//void txUintUart2(unsigned int i){
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, (unsigned char)(i>>24));
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, (unsigned char)((i&0xff0000)>>16));
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, (unsigned char)((i&0xff00)>>8));
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, (unsigned char)(i&0xff));
//
//}
//
//unsigned char rxCharUart2(unsigned char * b){
//    if(UARTReceivedDataIsAvailable (UART2)){
//        *b=UARTGetDataByte (UART2);
//        return 1;
//    }else return 0;
//}

//************ UART3 -->     *****************************
void initUart3(void){
    RPB9Rbits.RPB9R=0x01;//rpB9
    U3RXRbits.U3RXR=0x08;//rpb3
    UARTConfigure(UART3, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART3, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART3, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART3, GetPeripheralClock(), 115200);
    UARTEnable(UART3, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX|UART_RX));
//    PRN_RESET_High();
}

void txCharUart3(unsigned char b){
    while(!UARTTransmitterIsReady(UART3));
    UARTSendDataByte(UART3, b);
}

unsigned char rxCharUart3(unsigned char * b){
    if(UARTReceivedDataIsAvailable(UART3)){
        *b=UARTGetDataByte(UART3);
        return 1;
    }else return 0;
}

unsigned char dataReadyUart3(void){
    if(UARTReceivedDataIsAvailable ( UART3 ))return 1;else return 0;
}
unsigned char readCharUart3(void){
    return UARTGetDataByte(UART3);
}

//************************************************
//******** Scrive Buffer nella seriale ***********
//************************************************
void WriteUart3(byte* buf,byte  len){
	byte i=0; 	
	
	for(i=0;i<len;i++){
		txCharUart3(buf[i]);
	}
}


//************ UART2 --> stampante    *****************************
void initUart2(void){
    U2RXRbits.U2RXR=0x04;   //rx su pf1
    RPF0Rbits.RPF0R=1;      //tx su pf0
    //RPB9Rbits.RPB9R=0x01;//rpf1
    //U4RXRbits.U4RXR=0x02;//rpf4
    UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART2, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART2, GetPeripheralClock(), bps_UART_PRN);
    UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL |   UART_TX|UART_RX));
//    PRN_RESET_High();
}

void txCharUart2(unsigned char b){
    while(!UARTTransmitterIsReady(UART2));
    UARTSendDataByte(UART2, b);
}



void txStringUart3(char * b){
    unsigned char i;
    unsigned char idx=0;
    
    for(i=0;i<0xff;i++){
        if(b[i]==0)break;
    }
    while(idx<i){
        txCharUart3(b[idx]);
        idx++;
    }
}

////************ UART2 --> gas analyzer *****************************
//void initUart4(void){
//    RPB8Rbits.RPB8R=0x02;//rpB8
//    //U4RXRbits.U4RXR=0x02;//rpf4
//    UARTConfigure(UART4, UART_ENABLE_PINS_TX_RX_ONLY);
//    UARTSetFifoMode(UART4, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
//    UARTSetLineControl(UART4, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
//    UARTSetDataRate(UART4, GetPeripheralClock(), bps_UART_PRN);
//    UARTEnable(UART4, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
//
//}
//
//void txCharUart4(unsigned char b){
//    while(!UARTTransmitterIsReady(UART4));
//    UARTSendDataByte(UART4, b);
//}

////************ UART2 --> gas analyzer *****************************
//void initUart4(void){
//    RPB8Rbits.RPB8R=0x02;//rpB8
//    //U4RXRbits.U4RXR=0x02;//rpf4
//    UARTConfigure(UART4, UART_ENABLE_PINS_TX_RX_ONLY);
//    UARTSetFifoMode(UART4, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
//    UARTSetLineControl(UART4, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
//    UARTSetDataRate(UART4, GetPeripheralClock(), bps_UART_PRN);
//    UARTEnable(UART4, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
//
//}
//
//void txCharUart4(unsigned char b){
//    while(!UARTTransmitterIsReady(UART4));
//    UARTSendDataByte(UART4, b);
//}



