
//#include <peripheral\int.h>
#include <plib.h>

void  __attribute__((optimize("-O0"))) SysDelayUs(unsigned short us ){
    unsigned short w;
    for(;us>0;us--){
        for(w=0;w<4;w++)Nop();
    }
}

void  __attribute__((optimize("-O0"))) SysDelay05Us(void){
    Nop();
    Nop();
    Nop();
    Nop();
    Nop();
    Nop();
    Nop();
    Nop();
}

//void DelayMs(WORD ms)
//{
//    unsigned char i;
//    while(ms--)
//    {
//        i=4;
//        while(i--)
//        {
//            SysDelayUs(50);
//        }
//    }
//}

// Period needed for timer 1 to trigger an interrupt every 1 second
#define PERIOD  50000    //10ms
void Timer1Init(void){
// Configure Timer 1 using PBCLK as input, 1:256 prescaler
    // Period matches the Timer 1 frequency, so the interrupt handler
    // will trigger every one second...
    OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_8, PERIOD);

    // Set up the timer interrupt with a priority of 2
    INTEnable(INT_T1, INT_ENABLED);
    INTSetVectorPriority(INT_TIMER_1_VECTOR, INT_PRIORITY_LEVEL_2);
    INTSetVectorSubPriority(INT_TIMER_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    
    
}
