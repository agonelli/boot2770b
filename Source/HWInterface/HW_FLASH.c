
/*
 * File:   HW_FLASH.c
 * Author: matteob
 *
 * Created on 12 aprile 2013, 11.26
 */

#include "global.H"
#include "Compiler.h"
#include "GenericTypeDefs.h"
#include "HardwareProfile.h"
#include "HW_FLASH.h"
#include "HWGPIO.h"
#include "FlashDriver/sflashMX25L.h"
#include "spi.H" 
#include "lcdalfa.H"


void SFMX25L_16_32_64_SelectFlash(unsigned char mem){
    if (mem == MEM1){
        FCS_LOW();
    }else{
//        CSMEM1_Low();
    }
}

void SFMX25L_16_32_64_DeselectFlash(unsigned char mem){
    if (mem == MEM1){
        FCS_HIGH();
    }else{
//        CSMEM1_High();
    }
}

unsigned char initFlash(void){
    RPG8Rbits.RPG8R=0x06;//rpg8
    SDI2Rbits.SDI2R=0x01;//rpg7

    DRV_SPI_INIT_DATA strDRV_SPI_INIT_DATA;
    strDRV_SPI_INIT_DATA.baudRate = 4; //da controllare sul datasheet
    strDRV_SPI_INIT_DATA.channel = Channel_For_MX25L;
    strDRV_SPI_INIT_DATA.cke = SPI_CKE_ACT_IDLE;
    strDRV_SPI_INIT_DATA.ckp = SPI_CKP_ACT_HIGH;
    strDRV_SPI_INIT_DATA.mode = SPI_MODE_8BITS;
    strDRV_SPI_INIT_DATA.smp = SPI_SMP_PHASE_MID; //questo � starno
    DRV_SPI_Initialize(strDRV_SPI_INIT_DATA.channel, &strDRV_SPI_INIT_DATA);
    if(ID_MX25L == SFMX25L_16_32_64_GetId(MEM1))return 1;else return 0;
}

void WrByte(unsigned char val){
    SPIPut(Channel_For_MX25L, val);
    (void)SPIGet(Channel_For_MX25L);
}

unsigned char RdByte(void){
    SPIPut(Channel_For_MX25L, 0xFF);
    return SPIGet(Channel_For_MX25L);
}

void RdBytes(unsigned char * data, unsigned long len){
    while(len--){
        SPIPut(Channel_For_MX25L, 0xFF);
        *data++ = SPIGet(Channel_For_MX25L);
    }
}

unsigned int FreadUINT32(unsigned int fadd){
    unsigned char buf[4];
    unsigned int i;
    (void) SFMX25L_16_32_64_RdData(fadd,(void*) buf, 4,MEM1);
    i=buf[0];
    i=i<<8;
    i|=buf[1];
    i=i<<8;
    i|=buf[2];
    i=i<<8;
    i|=buf[3];
    return i;

}

unsigned short FreadUINT16(unsigned int fadd){
    unsigned char buf[2];
    unsigned short i;
    (void) SFMX25L_16_32_64_RdData(fadd,(void*) buf, 2,MEM1);
    i=buf[0];
    i=i<<8;
    i|=buf[1];
    return i;

}

unsigned short FreadUINT16u(unsigned int fadd){
    unsigned char buf[2];
    unsigned short i;
    (void) SFMX25L_16_32_64_RdData(fadd,(void*) buf, 2,MEM1);
    i=buf[1];
    i=i<<8;
    i|=buf[0];
    return i;

}

//===========================================================================
//Descrizione:	Legge un buffer dalla flash seriale
//				La lettura termina alla fine della pagina
//Parametri:		addr	= indirizzo flash
//				data	= puntatore ai dati
//				len		= numero byte
//Ritorno:		numero byte letti
//===========================================================================
word SFLASH_RdData(dword addr, byte* data, word len){
	byte	ad1, ad2, ad3;
	word	cnt;
	byte mem;

	if(addr<0x400000)mem=MEM1;
	else{
		addr=addr-0x400000;
		mem=MEM2;
		}
	if (len == 0)return(0);
	ad1 = (byte)((addr >> 16) & 0xff);
	ad2 = (byte)((addr >> 8) & 0xff);
	ad3 = (byte)(addr & 0xff);
	// invia comando lettura
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x03);		// comando lettura
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
	cnt = 0;
	while(cnt < len){
		*data++ = RdByte();
		cnt++;
		ad3++;
//		if (ad3 == 0)	// termina se fine pagina
//			break;
		}
	SFMX25L_16_32_64_DeselectFlash(mem);
	return(cnt);
}

//===========================================================================
//Descrizione:	Scrive un buffer nella flash seriale
//				La scrittura termina alla fine della pagina
//Parametri:		addr	= indirizzo flash
//				data	= puntatore ai dati
//				len		= numero byte
//Ritorno:		numero byte scritti max 0x100
//===========================================================================
word SFLASH_WrData(dword addr, byte* data, word len){
	byte	ad1, ad2, ad3;
	word	cnt;
	byte mem;
        dword add;

	if(addr<0x400000)mem=MEM1;
	else{
		addr=addr-0x400000;
		mem=MEM2;
		}
	if (len == 0)return(0);
        cnt = 0;
        for(add=addr;;){
            if (SFMX25L_16_32_64_ResetProt(mem))return(0);	// verifica protezione
            SFMX25L_16_32_64_WrEnable(mem);		// abilita scrittura
            ad1 = (byte)((add >> 16) & 0xff);
            ad2 = (byte)((add >> 8) & 0xff);
            ad3 = (byte)(add & 0xff);
            // invia comando scrittura pagina
            SFMX25L_16_32_64_SelectFlash(mem);
            WrByte(0x02);		// comando scrittura
            WrByte(ad1);		// indirizzo
            WrByte(ad2);
            WrByte(ad3);
            while (cnt < len) {
                    WrByte(*data++);
                    cnt++;
                    ad3++;
                    if (ad3 == 0){	// termina se fine pagina
                        add+=0x100;
                            break;
                    }
            }
            SFMX25L_16_32_64_DeselectFlash(mem);
            (void)SFMX25L_16_32_64_WaitEndWr(mem);
//            printdwlcd(RIGA4,add,9,0);
//            printwlcd(RIGA3,cnt,9,0);
//            printwlcd(RIGA2,len,9,0);

            if(cnt >= len)break;
        }
	if (SFMX25L_16_32_64_WaitEndWr(mem))	return(0);	// attesa scrittura
	return(cnt);
}

/*
===========================================================================
Descrizione:	Cancella un settore della flash seriale

Parametri:		settore	= settore da cancellare
Ritorno:		errore di cancellazione
					0 --> ok
					1 --> errore
*/
byte SFLASH_BlockErase(dword addr){
	byte	ad1, ad2, ad3;
        byte mem;
        if(addr<0x400000)mem=MEM1;
	else{
		addr=addr-0x400000;
		mem=MEM2;
		}
        if (SFMX25L_16_32_64_ResetProt(mem))return(0);	// verifica protezione
        SFMX25L_16_32_64_WrEnable(mem);		// abilita scrittura

	ad1 = (byte)((addr >> 16) & 0xff);
	ad2 = (byte)((addr >> 8) & 0xff);
	ad3 = (byte)(addr & 0xff);
	// invia comando cancella settore
        SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0xd8);		// comando
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
        SFMX25L_16_32_64_DeselectFlash(mem);

	// attesa cancellazione
	if (SFMX25L_16_32_64_WaitEndWr(mem))	return(0);	// attesa scrittura
	return(0);
}
