
#include "global.h"
#include "HWGPIO.h"
#include "usb_flash_int.h"
#include "MDD File System/FSIO.h"
#include <stdio.h>
#include "lcdalfa.h"
//#include "capprot/capprotMasterV2.h"
#include "dlelfaprot/DL_ElfaMB.h"
//#include "record.h"
//#include "utils.h"
#include "HWInterface/hw_flash.h"
//#include "ee.h"
#include "flashdriver/sflashMX25L.h"


UsbState stusb=usbstNop;
extern volatile unsigned long g_ulSysTickCount;
extern t_DL_ELFAMBRXCode DLEC;
extern unsigned short RXCharBufferCAP;
extern unsigned char * RXBuf;
extern unsigned short RXLen;
extern unsigned char * lpParameterVal;
extern char tmpString[];
extern const unsigned char nchar[];

char tmpFileName[15];
extern char gdgb[];
FSFILE * g_sFileObject;

unsigned char BufferForCopyBin[MX25L_MaxSizeBufferWrite];
#define MaxSizeBufferWrite MX25L_MaxSizeBufferWrite
unsigned char PrintFlashToDebugUart(const char * FileName, unsigned long AddFlashStart);
unsigned char CopyFlashToMicro(unsigned long AddFlashStart, unsigned long lenght);


typedef struct
{
    UINT8 *start;
    UINT8 len;
    UINT8 status;
}T_REC2;

#define ST_UPDATE_USBDETECT     0
#define ST_UPDATE_FSINIT        10
#define ST_UPDATE_FINDFILE      20
#define ST_UPDATE_FINDNEXT      25
#define ST_UPDATE_RUN           30
#define ST_UPDATE_END           40
#define ST_SEND_CMD             50
#define ST_RECEIVE_CMD          60
#define ST_UPDATE_FAIL          100
#define ST_UPDATE_SUCCESS       200


#define ENDOFUSERFLASH 0xE000
#define STARTUSERFLASH 0x100
#define STEPERASE 0x40

#define STX 2
#define ETX 3
extern const unsigned char MyCode[];
const char eol[] = {13,10};
#if 0
typedef struct {
    unsigned short ADDstr;
    unsigned short ADDvalue;
    t_um um;
} Contatori;
const Contatori ElencoStampe[] = {{E_TOTCAR,ADDCONTGASCAR,Grammi},
                                       {E_TOTREC,ADDCONTGASRECT,Grammi},
                                       {E_TOTREDFILT,ADDCONTGASREC,Grammi},
                                       {E_TOTACC,ADDCONTACC,Ore},
                                       {E_TOTVUOTO,ADDCONTPOMPT,Minuti},
                               };
#endif

//*****************************************************************************
// salva un file da chiavetta
//
//*****************************************************************************
#if 0
byte SaveFile(const char * filename,unsigned long AddFlashStart,unsigned long AddFlashEnd,byte timeout,byte flags){
    SearchRec rec;
    byte st = 0;
    USBPWR_ON();
    rt2=timeout;
    for(;;){
        gestsys();
        //printwlcd(RIGA1, st, 5, 0);
        USBTasks();

        switch (st) {
            case 0:
                if (USBHostMSDSCSIMediaDetect()) st = 10;
                if(key==KMENU)return 0;
                if(timeout){
                    if(rt2==0)st=100;
                }
                break;
            case 10:
                if (FSInit())st = 20;
                else st=100;;
                break;
            case 20:
                clrlcd();
                printlcd(RIGA1, "writing...          ");
                if (CopyFlashToBin(filename, AddFlashStart,AddFlashEnd-AddFlashStart) == 0) {
                    printlcd(RIGA1+11, "ERROR!          ");
                    st=100;
                }else  st=200;
                break;
            case 100:   //non ha aggiornato
                if(flags&POWEROFF)USBPWR_OFF();
                return 0;
                break;
            case 200:   //aggiornato ok
                if(flags&POWEROFF)USBPWR_OFF();
                return 1;
                break;
        }
    }
    if(flags&POWEROFF)USBPWR_OFF();
    return 0;
}
#endif

/*
 * @brief LoadFile: carica un file dal dongle USB
 */
#if 1
byte LoadFile(const char * filename,unsigned long AddFlashStart,byte timeout,byte flags) {
    SearchRec rec;
    byte st = 0;

#ifdef _USBDBG    
    sprintf(gdgb, "\nLoadFile:: USBPWR_ON");txStringUart3((unsigned char *)gdgb);
#endif
    USBPWR_ON();
    if(timeout==0){
        clrlcd();
        printlcd(RIGA1, "Search");
        printlcd(RIGA2, filename);
    }
    rt2=timeout;
    for(;;){
        //gestsys();
        //printwlcd(RIGA1, st, 5, 0);
        USBTasks();

#ifdef _USBDBG    
        if(st!=ST_UPDATE_USBDETECT){
            sprintf(gdgb, "\nLoadFile:: state(%d)",st);txStringUart3((unsigned char *)gdgb);
        }
#endif
        switch (st) {
            case ST_UPDATE_USBDETECT:
                if (USBHostMSDSCSIMediaDetect()) 
                    st = ST_UPDATE_FSINIT;
                //if(key==KMENU)
                //    return 0;
                if(timeout){
                    if(rt2==0)
                        st=ST_UPDATE_FAIL;
                }
                break;
            case ST_UPDATE_FSINIT:
                if (FSInit())
                    st = ST_UPDATE_FINDFILE;
                else 
                    st = ST_UPDATE_FAIL;
                break;
            case ST_UPDATE_FINDFILE:
                if (FindFirst(filename, ATTR_ARCHIVE, &rec) == FILEFOUND) {
                    clrlcd();
                    printlcd(RIGA1, "Search");
                    printlcd(RIGA2, filename);
                    printlcd(RIGA1+11, "FOUND");
                    st = ST_UPDATE_RUN;
                } else 
                    st = ST_UPDATE_FAIL;
                break;
            case ST_UPDATE_RUN:
                if (CopyBinToFlash(rec.filename, AddFlashStart) == 0) {
                    printlcd(RIGA1+11, "ERROR!          ");
                    rt2=20;
                    st=50;
                }else  
                    st = ST_UPDATE_SUCCESS;
#ifdef _USBDBG  
                //Verifica se il contenuto del file nel dongle corrisponde a quanto caricato in FLash
                sprintf(gdgb, "\nPrintFlashToDebugUart check if equal:: Dongle Vs Flash",st);txStringUart3((unsigned char *)gdgb);
                PrintFlashToDebugUart(rec.filename, AddFlashStart);
#endif
                break;
            case 50:
                if(rt2==0)
                    st=ST_UPDATE_FAIL;
                break;
            case ST_UPDATE_FAIL:
                if(flags&POWEROFF)
                    USBPWR_OFF();
                return 0;
                break;
            case ST_UPDATE_SUCCESS:
                if(flags&ERASEFILE)
                    (void)FSremove(filename);
                if(flags&POWEROFF)
                    USBPWR_OFF();
                
                CopyFlashToMicro(0, rec.filesize);
                
                return 1;
                break;
        }
    }
    if(flags&POWEROFF)USBPWR_OFF();

}
#endif


/*
 * @brief CopyBinToFlash: copia il file "FileName" sulla Flash all'indirizzo "AddFlashStart"
 */
unsigned char CopyBinToFlash(const char * FileName, unsigned long AddFlashStart){
    //static FSFILE * g_sFileObject;
    static unsigned long AddFlash;
    static size_t n,size,rval;
    static unsigned short w,i;

    g_sFileObject=FSfopen( FileName, FS_READ);
    size=1;
    n=MaxSizeBufferWrite;
    
    if(g_sFileObject != NULL){
        //printlcd(RIGA1,(char *)FileName);
        printlcd(RIGA1,"LOAD           %");

#ifdef _USBDBG    
        sprintf(gdgb, "\nCopyBinToFlash:: %s  size(0x%08x)\n",FileName,MaxSizeBufferWrite);txStringUart3((unsigned char *)gdgb);
#endif
        for (AddFlash = AddFlashStart; AddFlash < g_sFileObject->size + AddFlashStart;){
            if (AddFlash % SFMX25L_32_SectorSize == 0) {
#ifdef _USBDBG    
//                sprintf(gdgb, "\nErase Sector:: (0x%08x)",AddFlash);txStringUart3((unsigned char *)gdgb);
#endif
                //printlcd(RIGA2,"Uploading");
                (void)SFMX25L_16_32_64_SectorErase(AddFlash, MEM1);
                w=((AddFlash-AddFlashStart)*100)/g_sFileObject->size;
                printwlcd(RIGA1+12,w,3,ALIGNDX|NOZERO);
            }
            rval=FSfread(BufferForCopyBin, size,n, g_sFileObject);
#ifdef _USBDBG    
//            sprintf(gdgb, "\nRead:: rval=(0x%08x)",rval);txStringUart3((unsigned char *)gdgb);
#endif
            if(rval==n){
                if(SFLASH_WrData(AddFlash, BufferForCopyBin, MaxSizeBufferWrite)==0){
#ifdef _USBDBG    
                    sprintf(gdgb, "\nWrite Sector (0x%08x) rval=(0x%08x) ERROR!!",AddFlash,rval);txStringUart3((unsigned char *)gdgb);
#endif
                    FSfclose(g_sFileObject);
                    return 0;
                }
                AddFlash += MaxSizeBufferWrite;
            }else{
                if(rval!=0){
                    if(SFLASH_WrData(AddFlash, BufferForCopyBin, rval)==0){
#ifdef _USBDBG    
                        sprintf(gdgb, "\nWrite Sector (0x%08x) rval=(0x%08x) ERROR!!",AddFlash,rval);txStringUart3((unsigned char *)gdgb);
#endif
                        FSfclose(g_sFileObject);
                        return 0;
                    }
                }
#ifdef _USBDBG    
                sprintf(gdgb, "\nWrite FLASH SUCCESS!!");txStringUart3((unsigned char *)gdgb);
#endif
                FSfclose(g_sFileObject);
                return 1;
            }
        }
    }else{
        //FSfclose(g_sFileObject);
        return 0;
    }

#ifdef _USBDBG    
    sprintf(gdgb, "\nWrite FLASH SUCCESS!!");txStringUart3((unsigned char *)gdgb);
#endif
  FSfclose(g_sFileObject);
  return 1;
}



unsigned char CopyFlashToBin(const char * FileName, unsigned long AddFlashStart, unsigned long lenght){
    static FSFILE * g_sFileObject;
    unsigned long AddFlash;
////unsigned short usBytesRead;
//    char IdChar[40];
    size_t size,n,rval;
//
    //(void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
    g_sFileObject=FSfopen( FileName, FS_WRITEPLUS);
    size=1;
    n=MaxSizeBufferWrite;
    if(g_sFileObject != NULL){
        for (AddFlash = AddFlashStart; AddFlash < lenght + AddFlashStart;){
////            FL128P_RdData(AddFlash, BufferForCopyBin, MaxSizeBufferWrite,0);
            SFLASH_RdData((dword) AddFlash, BufferForCopyBin, MaxSizeBufferWrite);
            AddFlash += MaxSizeBufferWrite;
//            if((AddFlash%0x8000)==0x00){
//                sprintf(IdChar, "    Downloading: %d %%    ", (short)((float)(AddFlash - AddFlashStart)/(float)(lenght - AddFlashStart) * 100.0));
////                GrStringDrawCentered(&g_sContext, IdChar, -1, GrContextDpyWidthGet(&g_sContext) / 2, 32, 1);
//            }

            rval=FSfwrite(BufferForCopyBin, size,n, g_sFileObject);
            if(rval!=n){
//                (void)SetClockVars(2000+tmAnno, tmMese, tmGiorno, tmOre, tmMinuti, tmSecondi);
                FSfclose(g_sFileObject);
                return 0;
            }
        }
    }else{
        //FSfclose(g_sFileObject);
        return 0;
    }
//  (void)SetClockVars(2000+tmAnno, tmMese, tmGiorno, tmOre, tmMinuti, tmSecondi);
  FSfclose(g_sFileObject);
  return 1;
}


unsigned char CopyFlashToMicro(unsigned long AddFlashStart, unsigned long lenght){
    UINT  lreadBytes;
    UINT8 lasciiBuffer[1024];
    int rval=0;
    int i;
    T_REC2 lrecord;

    
    unsigned long AddFlash;
    
    printlcd(RIGA1,"ERASE FLASH");
    EraseFlash();
    // Initialize the state-machine to read the records.
    lrecord.status = REC_NOT_FOUND;
    printlcd(RIGA2,"PROGRAM FLASH");
    
        for (AddFlash = AddFlashStart; AddFlash < lenght + AddFlashStart;){
            SFLASH_RdData((dword) AddFlash, &lasciiBuffer[0], 512);
            AddFlash += 512;

            for(i = 0; i < (readBytes /*+ pointer*/); i++)
            {
                //This state machine seperates-out the valid hex records from the read 512 bytes.
                switch(lrecord.status)
                {
                    case REC_FLASHED:
                    case REC_NOT_FOUND:
                        if(lasciiBuffer[i] == ':')
                        {
                            // We have a record found in the 512 bytes of data in the buffer.
                            lrecord.start = &lasciiBuffer[i];
                            lrecord.len = 0;
                            lrecord.status = REC_FOUND_BUT_NOT_FLASHED;
                        }
                        break;
                    case REC_FOUND_BUT_NOT_FLASHED:
                        if((lasciiBuffer[i] == 0x0A) || (lasciiBuffer[i] == 0xFF))
                        {
                            // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                            // Start the hex conversion from element
                            // 1. This will discard the ':' which is
                            // the start of the hex record.
                            ConvertAsciiToHex(&record.start[1],hexRec);
                            WriteHexRecord2Flash(hexRec);
                            lrecord.status = REC_FLASHED;
                        }
                        break;
                }
                // Move to next byte in the buffer.
                lrecord.len ++;
            }
        }

        return 1;
}




#if 0
unsigned char savefileEE(const char * FileName, unsigned long AddEEStart, unsigned long AddEEEnd){
    unsigned long ADDEE = SFADD_RECORD;
    unsigned long Last_ADDEE;
    unsigned short w;
    unsigned char buf[256];
    static FSFILE * g_sFileObject;
    size_t size, rval;
    rt2 = 50;
    USBPWR_ON();
    clrlcd();
    printlcd(RIGA1,"wait");
    for (;;) {
        USBTasks();
        if (USBHostMSDSCSIMediaDetect()) {
            if (FSInit()) {
                Last_ADDEE = readDWordEE(EEADD_NEXTRECORDADD);
                (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
                g_sFileObject = FSfopen("EEimg.bin", FS_WRITEPLUS);
                size = 1;
                if (g_sFileObject == NULL) {
                    USBPWR_OFF();
                    return 0;
                }

                for(ADDEE=0x0000;ADDEE<0x7fff;ADDEE+=256){
                    for(w=0;w<256;w++){
                        buf[w]=readByteEE(ADDEE+w);
                    }
                    rval = FSfwrite(buf, size, 256, g_sFileObject);
                    printwlcd(RIGA2,ADDEE,5,0);
                    if (rval != 256) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 0;
                    }
                }
                (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
                FSfclose(g_sFileObject);
                USBPWR_OFF();
                return 1;
            } else if (rt2 == 0)return 33;
        } else if (rt2 == 0)return 34;
    }
    return 1;
}

unsigned char loadfileEE(const char * FileName, unsigned long AddEEStart, unsigned long AddEEEnd) {
    SearchRec rec;
    byte st = 0;
    size_t size,n,rval;
    unsigned long dw;
    unsigned short w;

    USBPWR_ON();
    clrlcd();
    printlcd(RIGA1, "Search");
    printlcd(RIGA2, FileName);
    rt2=50;
    for(;;){
        gestsys();
        USBTasks();

        switch (st) {
            case 0:
                if (USBHostMSDSCSIMediaDetect()) st = 10;
                //if(key==KMENU)return 0;
                if(rt2==0)st=100;
                break;
            case 10:
                if (FSInit())st = 20;
                else st=100;;
                break;
            case 20:
                if (FindFirst(FileName, ATTR_ARCHIVE, &rec) == FILEFOUND) {
                    clrlcd();
                    printlcd(RIGA1, "Search");
                    printlcd(RIGA2, FileName);
                    printlcd(RIGA1+11, "FOUND");
                    st = 30;
                } else st=100;
                break;
            case 30:
                g_sFileObject=FSfopen( FileName, FS_READ);
                size=1;
                n=MaxSizeBufferWrite;
                if(g_sFileObject != NULL){
                    //printlcd(RIGA1,(char *)FileName);
                    printlcd(RIGA1,"LOAD           ");
                    for (dw = AddEEStart; dw < g_sFileObject->size + AddEEStart;){
                        printdwlcd(RIGA1+11,dw ,5,ALIGNDX|NOZERO);
                        rval=FSfread(BufferForCopyBin, size,n, g_sFileObject);
                        if(rval==n){
                            //SFLASH_WrData(AddFlash, BufferForCopyBin, MaxSizeBufferWrite);
                            for(w=0;w<MaxSizeBufferWrite;w++)writeByteEE(BufferForCopyBin[w],dw+w );
                            dw += MaxSizeBufferWrite;
                        }else{
                            if(rval!=0){
                                for(w=0;w<rval;w++)writeByteEE(BufferForCopyBin[w],dw+w );
                            }
                            FSfclose(g_sFileObject);
                            return 1;
                        }
                    }
                    st=200;
                }else{
                    //FSfclose(g_sFileObject);
                    return 0;
                }
              FSfclose(g_sFileObject);

                break;
            case 100:   //non ha aggiornato
                USBPWR_OFF();
                return 0;
                break;
            case 200:   //aggiornato ok
                USBPWR_OFF();
                return 1;
                break;
        }
    }
    USBPWR_OFF();
}

//*****************************************************************************
// salva servizi
//*****************************************************************************
void saveServices(void){
//	char fn[13];
//	byte i;
//
////	for(i=0;i<8;i++)fn[i]=readByteEE(SERIALNUMBER+i);
//	fn[8]='.';
//	fn[9]='h';
//	fn[10]='s';
//	fn[11]='t';
//	fn[12]=0;
//
//	clrlcd();
////	menu(154);
////	WriteDWordEE(ADDDATAEXPSERV,readUClock());
//	if(savefileEE(fn,0x0000,0x40000)==0){
////		menu(188);
////		i=getkey(WAITNOKEY);
//	}
}

//*****************************************************************************
// salva servizi
//*****************************************************************************
void saveLastService(void){
//	char fn[13];
//	byte i;
//	word contaservizi;
//	dword add;
//
////	contaservizi=readWordEE(ADDCONTASERVIZI);
////	add=contaservizi;
////	add=add*(NBYTERECORD);
////
////	contaservizi=readWordEE(ADDCONTASERVIZI);
////	for(i=0;i<8;i++)fn[i]=readByteEE(SERIALNUMBER+i);
//	fn[0]='l';
//	fn[1]='a';
//	fn[2]='s';
//	fn[3]='t';
//	fn[4]='-';
//	fn[5]='j';
//	fn[6]='o';
//	fn[7]='b';
//	fn[8]='.';
//	fn[9]='h';
//	fn[10]='s';
//	fn[11]='t';
//	fn[12]=0;
//
//	clrlcd();
////	menu(154);
//	//WriteDWordEE(ADDDATAEXPSERV,readUClock());
//	if(savefileEE(fn,BASESTORICO+add,BASESTORICO+add+NBYTERECORD)==0){
////		menu(188);
////		i=getkey(WAITNOKEY);
//	}
}
#endif
void fillgpbuf(void){
	unsigned char i;
	for(i=0;i<16;i++){
		if(gpbuf[i]==0){
			break;
		}
	}
	for(;i<16;i++){
		gpbuf[i] = ' ';
	}
}
//**********************************************************************************************************************************
//***** salva servizi su usb *******************************************************************************************************
//**********************************************************************************************************************************
#if 0
unsigned char SaveRecordToSD(void) {
    unsigned long ADDEE = SFADD_RECORD;
    unsigned long Last_ADDEE;
    unsigned char fresult = 1;
    unsigned short w;
    unsigned long dw;
    unsigned char v;
    unsigned char Flag;
    static FSFILE * g_sFileObject;
    size_t size, rval;
    rt2 = 50;
    USBPWR_ON();

    for (;;) {
        USBTasks();
        if (USBHostMSDSCSIMediaDetect()) {
            if (FSInit()) {
                Last_ADDEE = readDWordEE(EEADD_NEXTRECORDADD);
                (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
                g_sFileObject = FSfopen("Record.txt", FS_WRITEPLUS);
                size = 1;

                //	fresult = f_open(&g_sFileObject, "Record.txt", FA_CREATE_ALWAYS | FA_WRITE);
                //	if (fresult != FR_OK){
                //		return fresult;
                //	}
                if (g_sFileObject == NULL) {
                    USBPWR_OFF();
                    return 1;
                }

                /* Intestazione */
                FillStrFlash(gpbuf, E_ESP, LANGADD);

                rval = FSfwrite(gpbuf, size, nchar[E_ESP], g_sFileObject);
                if (rval != nchar[E_ESP]) {
                    FSfclose(g_sFileObject);
                    USBPWR_OFF();
                    return 2;
                }

                //	fresult |= f_write(&g_sFileObject,gpbuf, nchar[E_ESP], &NByteWrite);
                //	if (fresult != FR_OK){
                //		return fresult;
                //	}
                /*data*/
                FillStrDate(gpbuf, 0);
                rval = FSfwrite(gpbuf, size, 8, g_sFileObject);
                if (rval != 8) {
                    FSfclose(g_sFileObject);
                    USBPWR_OFF();
                    return 3;
                }
                //        fresult |= f_write(&g_sFileObject, gpbuf, 8, &NByteWrite);
                rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                if (rval != sizeof (eol)) {
                    FSfclose(g_sFileObject);
                    USBPWR_OFF();
                    return 4;
                }
                rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                if (rval != sizeof (eol)) {
                    FSfclose(g_sFileObject);
                    USBPWR_OFF();
                    return 5;
                }
                //	fresult |= f_write(&g_sFileObject, eol, sizeof(eol), &NByteWrite);
                //    fresult |= f_write(&g_sFileObject, eol, sizeof(eol), &NByteWrite);
//****************************************************************************************************************
/*informazioni officina*/
//****************************************************************************************************************
                Flag = 0;
                for (w = ADDENDPRINT1; w <= ADDENDPRINT6; w += 16) {
                    for (v = 0; v < 16; v++)gpbuf[v] = readByteEE(w + v);
                    if ((unsigned char) (gpbuf[0]) != 0xFF) {
                        if (Flag == 0) {
                            Flag = 1;
                            FillStrFlash(&gpbuf[32], EDIT1, LANGADD);
                            rval = FSfwrite(&gpbuf[32], size, nchar[EDIT1], g_sFileObject);
                            if (rval != nchar[EDIT1]) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 6;
                            }
                            //	            fresult |= f_write(&g_sFileObject,&gpbuf[32], nchar[EDIT1], &NByteWrite);
                            rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                            if (rval != sizeof (eol)) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 7;
                            }
                            //                fresult |= f_write(&g_sFileObject,eol, sizeof(eol), &NByteWrite);
                        }
                        rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                        if (rval != 16) {
                            FSfclose(g_sFileObject);
                            USBPWR_OFF();
                            return 8;
                        }
                        //        	fresult |= f_write(&g_sFileObject,gpbuf, 16, &NByteWrite);
                        rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                        if (rval != sizeof (eol)) {
                            FSfclose(g_sFileObject);
                            USBPWR_OFF();
                            return 8;
                        }
                        //fresult |= f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                    }
                }
//****************************************************************************************************************
                rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                if (rval != sizeof (eol)) {
                    FSfclose(g_sFileObject);
                    USBPWR_OFF();
                    return 9;
                }
                //fresult |= f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);

//****************************************************************************************************************
/* contatori */
//****************************************************************************************************************
                for (v = 0; v<sizeof (ElencoStampe) / sizeof (ElencoStampe[0]); v++) {
                    FillStrFlash(gpbuf, ElencoStampe[v].ADDstr, LANGADD);
                    fillgpbuf();
                    rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                    if (rval != 16) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 10;
                    }
                    // fresult |= f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                    dw = readDWordEE(ElencoStampe[v].ADDvalue);
                    switch (ElencoStampe[v].um) {
                        case Grammi:
                            if (udm != 0) {
                                dw = dw * 22046;
                                dw = dw / 10000;
                            }
                            dw /= 1000;
                            break;
                        case Minuti:
                            dw /= 60;
                            break;
                        case Ore:
                            break;
                    }
                    FillStrdw(gpbuf, dw, 8, NOZERO | ALIGNDX);
                    rval = FSfwrite(gpbuf, size, 8, g_sFileObject);
                    if (rval != 8) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 11;
                    }
                    //f_write(&g_sFileObject, gpbuf, 8, &NByteWrite);
                    switch (ElencoStampe[v].um) {
                        case Grammi:
                            FillStrFlash(&gpbuf[1], STRKG + udm, LANGADD);
                            break;
                        case Minuti:
                        case Ore:
                            gpbuf[1] = 'h';
                            gpbuf[2] = ' ';
                            break;
                    }
                    gpbuf[0] = ' ';
                    rval = FSfwrite(gpbuf, size, 3, g_sFileObject);
                    if (rval != 3) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 12;
                    }
                    //        fresult = f_write(&g_sFileObject, gpbuf, 3, &NByteWrite);
                    rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                    if (rval != sizeof (eol)) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 13;
                    }
                    //        fresult |= f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);

                }
                //    if (fresult != FR_OK) {
                //        return fresult;
                //    }
                for (v = 0; v < 3; v++) {
                    rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                    if (rval != sizeof (eol)) {
                        FSfclose(g_sFileObject);
                        USBPWR_OFF();
                        return 14;
                    }
                    //        fresult |= f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                }
                ClockEnable = 1;
//                printlcd(RIGA2, "OK");
//                printdwlcd(RIGA2, ADDEE, 8, 0);
//                printdwlcd(RIGA2 + 8, Last_ADDEE, 8, 0);
//*******************************************************************************************************************************************
//*******************************************************************************************************************************************
//*******************************************************************************************************************************************
                if (ADDEE < Last_ADDEE) {
                    while (ADDEE < Last_ADDEE) {
                        gestsys();
                        //printlcd(RIGA2,"OK1        ");
                        SFLASH_RdData(ADDEE++, (byte*) & gpbuf[32], 1);
                        if (gpbuf[32] == STX) {
                            /* Inizio ciclo MAN/AUTO */
                            SFLASH_RdData(ADDEE++, (byte*) & gpbuf[32], 1);
                            switch (gpbuf[32]) {
                                case REC_MAN:
                                    FillStrFlash(gpbuf, MANUALE, LANGADD);
                                    break;
                                case REC_AUTO:
                                    FillStrFlash(gpbuf, CICLOAUTO, LANGADD);
                                    break;
                                case REC_LAV:
                                    FillStrFlash(gpbuf, LAVAGGIO1, LANGADD);
                                    break;
                            }
                            //printlcd(RIGA2,"OK2        ");
                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                            if (rval != 16) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 15;
                            }
                            //fresult |= f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                            ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&RTC), sizeof (RTCTime));
                            FillStrTime(gpbuf);
                            gpbuf[5] = ' ';
                            FillStrDate(&gpbuf[6], 0);
                            gpbuf[15] = MyCode[pos_Separatore];
                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                            if (rval != 16) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 16;
                            }

                            //            fresult |= f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                            FillStrFlash(gpbuf, TARGAAUTO, LANGADD);
                            fillgpbuf();
                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                            if (rval != 16) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 17;
                            }
                            //            fresult |= f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                            /*targa*/
                            ADDEE += SFLASH_RdData(ADDEE, (byte*) gpbuf, 16);
                            fillgpbuf();
                            gpbuf[15] = MyCode[pos_Separatore];
                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                            if (rval != 16) {
                                FSfclose(g_sFileObject);
                                USBPWR_OFF();
                                return 18;
                            }
                            //            fresult |= f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                            //            if (fresult != FR_OK) {
                            //                return fresult;
                            //            }
                            while (ADDEE < Last_ADDEE) {
                                /* RECORD */
                                gestsys();
                                SFLASH_RdData(ADDEE, (byte*) & gpbuf[33], 1);
                                if (gpbuf[33] == ETX) {
                                    ADDEE++;
                                    rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                                    if (rval != sizeof (eol)) {
                                        FSfclose(g_sFileObject);
                                        USBPWR_OFF();
                                        return 19;
                                    }
                                    //               fresult = f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                                    break;
                                } else if (gpbuf[33] == STX) {
                                    //Errore record persi
                                    rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                                    if (rval != sizeof (eol)) {
                                        FSfclose(g_sFileObject);
                                        USBPWR_OFF();
                                        return 20;
                                    }
                                    //fresult = f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                                    FillStrFlash(gpbuf, REC_PERSI, LANGADD);
                                    rval = FSfwrite(gpbuf, size, nchar[REC_PERSI], g_sFileObject);
                                    if (rval != nchar[REC_PERSI]) {
                                        FSfclose(g_sFileObject);
                                        USBPWR_OFF();
                                        return 21;
                                    }
                                    //                    fresult = f_write(&g_sFileObject, gpbuf, nchar[REC_PERSI], &NByteWrite);
                                    rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                                    if (rval != sizeof (eol)) {
                                        FSfclose(g_sFileObject);
                                        USBPWR_OFF();
                                        return 22;
                                    }
                                    //                    fresult = f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                                    break;
                                } else {
                                    ADDEE++;
                                    ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&RTC), sizeof (RTCTime));
                                    switch (gpbuf[33]) {
                                        case FaseRecupero:
                                            ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&Recupero), sizeof (t_Recupero));
                                            FillStrFlash(gpbuf, RECUPERO1, LANGADD);
                                            break;
                                        case FaseVuoto:
                                            ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&Vuoto), sizeof (t_Vuoto));
                                            FillStrFlash(gpbuf, VUOTO1, LANGADD);
                                            break;
                                        case FaseCarica:
                                            ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&Carica), sizeof (t_Carica));
                                            FillStrFlash(gpbuf, CARICA, LANGADD);
                                            break;
                                        case FaseLavaggio:
                                            ADDEE += SFLASH_RdData(ADDEE, (unsigned char *) (&Lavaggio), sizeof (t_Lavaggio));
                                            break;
                                        default:
                                            //Errore codice fase non riconosciuto
                                            FillStrFlash(gpbuf, ERR_COD_FASE, LANGADD);
                                            rval = FSfwrite(gpbuf, size, nchar[ERR_COD_FASE], g_sFileObject);
                                            if (rval != nchar[ERR_COD_FASE]) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 23;
                                            }
                                            //                        fresult = f_write(&g_sFileObject, gpbuf, nchar[ERR_COD_FASE], &NByteWrite);
                                            rval = FSfwrite(eol, size, sizeof (eol), g_sFileObject);
                                            if (rval != sizeof (eol)) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 24;
                                            }
                                            //                       fresult = f_write(&g_sFileObject, eol, sizeof (eol), &NByteWrite);
                                            break;
                                    }
                                    if (gpbuf[33] != FaseLavaggio) {
                                        fillgpbuf();
                                        rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                        if (rval != 16) {
                                            FSfclose(g_sFileObject);
                                            USBPWR_OFF();
                                            return 25;
                                        }
                                        //                        fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                    }
                                    switch (gpbuf[33]) {
                                        case FaseRecupero:
                                            FillStrFlash(gpbuf, GASNAME, LANGADD);
                                            FillStrw(&gpbuf[nchar[GASNAME] - 1], Recupero.pRecEffettivo, 5, NOZERO | NEGATIVO | KG);
                                            FillStrFlash(&gpbuf[nchar[GASNAME] + 5], STRKG + udm, LANGADD);
                                            fillgpbuf();
                                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                            if (rval != 16) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 26;
                                            }
                                            //fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                            if (CellaOlioOut) {
                                                FillStrFlash(gpbuf, STANDBY2, LANGADD);
                                                FillStrw(&gpbuf[9], Recupero.pOilScar, 4, ALIGNDX | NOZERO | NEGATIVO | GR);
                                                FillStrFlash(&gpbuf[13], STRGR + udm, LANGADD);
                                                //fillgpbuf();
                                                gpbuf[15] = ' ';
                                                rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                                if (rval != 16) {
                                                    FSfclose(g_sFileObject);
                                                    USBPWR_OFF();
                                                    return 27;
                                                }
                                                //                              fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                            }
                                            break;
                                        case FaseVuoto:
                                            FillStrFlash(gpbuf, VUOTO2, LANGADD);
                                            FillStrw(&gpbuf[16 - 7], Vuoto.tVuotoEffettivo, 5, NOZERO | NEGATIVO);
                                            gpbuf[16 - 2] = 39;
                                            gpbuf[15] = ' ';
                                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                            if (rval != 16) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 28;
                                            }
                                            //                        fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                            memcpy(gpbuf, Str16Blanck, 16);
                                            FillStrFlash(gpbuf, ENDVUOTO2, LANGADD);
                                            switch (Vuoto.exitcode) {
                                                case ec_OK:
                                                    memcpy(&gpbuf[10], strOK, 3);
                                                    break;
                                                case ec_Skip:
                                                    memcpy(&gpbuf[10], strSKIP, 5);
                                                    break;
                                                case ec_Alarm:
                                                    memcpy(&gpbuf[10], strALARM, 6);
                                                    break;
                                            }
                                            fillgpbuf();
                                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                            if (rval != 16) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 29;
                                            }
                                            //                        fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                            break;
                                        case FaseCarica:
                                            if (CellaOlioIn) {
                                                FillStrFlash(gpbuf, OLIO1, LANGADD);
                                                FillStrw(&gpbuf[9], Carica.pOlioCar, 4, ALIGNDX | NOZERO | NEGATIVO | GR);
                                                FillStrFlash(&gpbuf[13], STRGR + udm, LANGADD);
                                                rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                                if (rval != 16) {
                                                    FSfclose(g_sFileObject);
                                                    USBPWR_OFF();
                                                    return 30;
                                                }
                                            }
                                            if (CellaUvIn) {
                                                FillStrFlash(gpbuf, UV1, LANGADD);
                                                FillStrw(&gpbuf[9], Carica.pUvCar, 4, ALIGNDX | NOZERO | NEGATIVO | GR);
                                                FillStrFlash(&gpbuf[13], STRGR + udm, LANGADD);
                                                rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                                if (rval != 16) {
                                                    FSfclose(g_sFileObject);
                                                    USBPWR_OFF();
                                                    return 30;
                                                }
                                            }


                                            FillStrFlash(gpbuf, GASNAME, LANGADD);
                                            FillStrw(&gpbuf[nchar[GASNAME] - 1], Carica.pCarEffettivo, 5, NOZERO | NEGATIVO | KG);
                                            FillStrFlash(&gpbuf[nchar[GASNAME] + 5], STRKG + udm, LANGADD);
                                            fillgpbuf();
                                            rval = FSfwrite(gpbuf, size, 16, g_sFileObject);
                                            if (rval != 16) {
                                                FSfclose(g_sFileObject);
                                                USBPWR_OFF();
                                                return 31;
                                            }
                                            //                        fresult = f_write(&g_sFileObject, gpbuf, 16, &NByteWrite);
                                            break;
                                        case FaseLavaggio:

                                            break;
                                    }
                                    if (gpbuf[33] != FaseLavaggio) {
                                        FillStrTime(gpbuf);
                                        gpbuf[5] = ' ';
                                        FillStrDate(&gpbuf[6], 0);
                                        fillgpbuf();
                                        rval = FSfwrite(gpbuf, size, 15, g_sFileObject);
                                        if (rval != 15) {
                                            FSfclose(g_sFileObject);
                                            USBPWR_OFF();
                                            return 32;
                                        }
                                        //                    fresult = f_write(&g_sFileObject, gpbuf, 15, &NByteWrite);
                                    }
                                }
                            }
                            //            if (fresult != FR_OK) {
                            //                break;
                            //            }
                        } else {
                            ClearRecord();
                            fresult = 20;
                            break;
                        }
                    }
                }
                (void) SetClockVars(2000 + RTC.RTC_Year, RTC.RTC_Mon, RTC.RTC_Mday, RTC.RTC_Hour, RTC.RTC_Min, RTC.RTC_Sec);
                FSfclose(g_sFileObject);
                USBPWR_OFF();
                return 0;
            } else if (rt2 == 0)return 33;
        } else if (rt2 == 0)return 34;
    }
    //    f_close(&g_sFileObject);
    return 35;
}

void PrintBuffer(unsigned long add, char * buff, unsigned long size){
    unsigned int i;
    for(i=0;i<size;i++){
        if((i%16)==0){
            sprintf(gdgb, "\n0x%08x:: ",add+i);txStringUart3((unsigned char *)gdgb);
        }
        sprintf(gdgb, "%02x ",buff[i]);txStringUart3((unsigned char *)gdgb);
    }
}

/*
 * @brief PrintFlashToDebugUart: stampa il contenuto della Flash sulla seriale di debug
 */
unsigned char PrintFlashToDebugUart(const char * FileName, unsigned long AddFlashStart){
    static unsigned long AddFlash;
    static size_t n,size,rval;
    static unsigned short w,i;
    unsigned char lBufferForReadFlash[MX25L_MaxSizeBufferWrite];
    unsigned char ret=1;

    g_sFileObject=FSfopen( FileName, FS_READ);
    size=1;
    n=MaxSizeBufferWrite;
    
    if(g_sFileObject != NULL){
        for (AddFlash = AddFlashStart; AddFlash < g_sFileObject->size + AddFlashStart;){
            rval=FSfread(BufferForCopyBin, size,n, g_sFileObject);
            //PrintBuffer(AddFlash,BufferForCopyBin,n);

            if((AddFlash%0x8000)==0){
                sprintf(gdgb, "\n0x%08x 0x%08x ",g_sFileObject->size,AddFlash);txStringUart3((unsigned char *)gdgb);
            }

            if(rval==n){
                SFLASH_RdData((dword) AddFlash, lBufferForReadFlash, MaxSizeBufferWrite);
                //PrintBuffer(AddFlash,lBufferForReadFlash,n);
                if(memcmp(&BufferForCopyBin[0],&lBufferForReadFlash[0],MaxSizeBufferWrite)!=0){
                    sprintf(gdgb, "\nCMPARE FLASH ERROR1 (0x%08x)!!",AddFlash);txStringUart3((unsigned char *)gdgb);
                    ret=0;
                }
                AddFlash += MaxSizeBufferWrite;
            }else{
                if(rval!=0){
                    SFLASH_RdData((dword) AddFlash, lBufferForReadFlash, rval);
                    if(memcmp(&BufferForCopyBin[0],&lBufferForReadFlash[0],rval)!=0){
                        sprintf(gdgb, "\nCMPARE FLASH ERROR2(0x%08x)!!",AddFlash);txStringUart3((unsigned char *)gdgb);
                        ret = 0;
                    }
                }
            }
        }
    }
    else{
        ret=0;
    }

    if(ret==0){
#ifdef _USBDBG    
        sprintf(gdgb, "\nCMPARE FLASH ERROR!!");txStringUart3((unsigned char *)gdgb);
#endif
    }
    else{
#ifdef _USBDBG    
        sprintf(gdgb, "\nCMPARE FLASH SUCCESS!!");txStringUart3((unsigned char *)gdgb);
#endif
    }

    FSfclose(g_sFileObject);
    return ret;
}
#endif



