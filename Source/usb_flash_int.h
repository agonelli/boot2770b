/* 
 * File:   usb_flash_int.h
 * Author: andrea
 *
 * Created on 16 aprile 2013, 9.49
 */

#ifndef USB_FLASH_INT_H
#define	USB_FLASH_INT_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef enum {
	usbstNop,              //0
        usbstSearchDevice,
        usbstOpenFS,
        usbstSearchPar,
        usbstLoadPar,
        usbstSearchLang,
        usbstLoadLang,
        usbstSearchDB,
        usbstCheckDB,
        usbstCheckDBError,
        usbstLoadDB,
	usbstSearchSCO,
        usbstLoadSCO,
	usbstSearchCAPFW,
        usbstLoadCAPFW,
	usbstError,
} UsbState;



byte LoadFile(const char * filename,unsigned long AddFlashStart,byte timeout,byte flags);
byte SaveFile(const char * filename,unsigned long AddFlashStart,unsigned long AddFlashEnd,byte timeout,byte flags);

unsigned char CopyBinToFlash(const char * FileName, unsigned long AddFlashStart);
unsigned char CopyFlashToBin(const char * FileName, unsigned long AddFlashStart, unsigned long lenght);
unsigned char loadfileEE(const char * FileName, unsigned long AddEEStart, unsigned long AddEEEnd);
unsigned char savefileEE(const char * FileName, unsigned long AddEEStart, unsigned long AddEEEnd);




#define FILEFOUND 0
#define FILENOTFOUND -1

#define POWEROFF 0x01
#define ERASEFILE 0x02

#ifdef	__cplusplus
}
#endif

#endif	/* USB_FLASH_INT_H */

