//*** Tipi di Dato
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int dword;


#define _DECL_LCD20X4

#include "HWGPIO.h"
#include "hwtimer.h"
#include "lcd20x4.h"

//***** flcd ******
#define FLCDRS		0x01

extern byte bcdToByte(byte b);
extern void wait10ms(byte t);


//****************************************************
//***  waitbflcd  ************************************
//****************************************************
void waitbflcd(void){
    LCDBUS_IN();
    LCDRS_OFF();
    LCDRW_ON();
    for(;;){
	SysDelayUs(2);
	LCDEN_ON();
	SysDelayUs(2);
        if(LCDNOTBUSY())break;
  	LCDEN_OFF();
    }
    LCDEN_OFF();
    LCDBUS_OUT();
    SysDelayUs(2);
}

//**************************************************************
//***  setta cursore  ******************************************
//**************************************************************
/*
void setCursor(byte add,byte flags){
//	flags1&=(~FIOINT);
	waitbflcd();
	writelcd(0,0x80|add);
	waitbflcd();
	writelcd(0,0x8|flags);
	waitbflcd();
//	flags1|=FIOINT;
	
}
*/


//**************************************************************
//***  Print word LCD ******************************************
//************************************************************** 
void printwlcd(byte add,word w,byte nchar,byte flags) {
    char s[6];
    byte f = 0;
    byte i;
    byte temp;
    dword dw;

    if (w & 0x8000) {
        if (flags & NEGATIVO) {
            f |= NEGATIVO;
            w = 0xffff - w + 1;
        }
    }
    dw = (dword) w;// + ((dword) (flags & 3) << 16);
    s[5] = 0;

    s[0] = (char) (dw / 10000) + '0'; //converte in digit
    dw = dw % 10000;
    s[1] = (char) (dw / 1000) + '0';
    dw = dw % 1000;
    s[2] = (char) (dw / 100) + '0';
    dw = dw % 100;
    s[3] = (char) (dw / 10) + '0';
    s[4] = (dw % 10 + '0');
    if (nchar == 4) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 4; i++)s[i] = s[i + 1];
        }
        s[4] = 0;
    } else if (nchar == 3) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 3; i++)s[i] = s[i + 2];
        }
        s[3] = 0;
    } else if (nchar == 2) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 2; i++)s[i] = s[i + 3];
        }
        s[2] = 0;
    }
    if (flags & TRIMZERO) {
        for (i = 0; i < (nchar - 1); i++) {
            if (s[i] != '0')break;
        }
        for (temp = 0; i < nchar; i++) {
            s[temp] = s[i];
            temp++;
        }
        for (; temp < nchar; temp++) {
            s[temp] = ' ';
        }
    }
    if (flags & NOZERO) {
        for (i = 0; i < (nchar - 1); i++) {
            if (s[i] == '0')s[i] = ' ';
            else break;
        }
    }
 //   if (f & NEGATIVO)s[0] = '-';
 //   if (flags & FVDMAX)s[0] = '>';

    //	if(flags&TOPRN)sendPrn(s);else
    printlcd(add, s);
}

//**************************************************************
//***  Print dword LCD *****************************************
//**************************************************************
/*
void printdwlcd(byte add,dword dw,byte nchar,byte flags) {
    char s[11];
    byte f = 0;
    byte i;
    byte temp;

    if (dw & 0x80000000) {
        if (flags & NEGATIVO) {
            f |= NEGATIVO;
            dw = 0xffffffff - dw + 1;
        }
    }
    for (i = 0; i < 11; i++)s[i] = 0;

    s[0] = (char) (dw / 1000000000) + '0'; //converte in digit
    dw = dw % 1000000000;
    s[1] = (char) (dw / 100000000) + '0';
    dw = dw % 100000000;
    s[2] = (char) (dw / 10000000) + '0';
    dw = dw % 10000000;
    s[3] = (char) (dw / 1000000) + '0';
    dw = dw % 1000000;
    s[4] = (char) (dw / 100000) + '0';
    dw = dw % 100000;
    s[5] = (char) (dw / 10000) + '0';
    dw = dw % 10000;
    s[6] = (char) (dw / 1000) + '0';
    dw = dw % 1000;
    s[7] = (char) (dw / 100) + '0';
    dw = dw % 100;
    s[8] = (char) (dw / 10) + '0';
    s[9] = (dw % 10 + '0');

    if (nchar == 9) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 9; i++)s[i] = s[i + 1];
        }
        s[9] = 0;
    } else if (nchar == 8) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 8; i++)s[i] = s[i + 2];
        }
        s[8] = 0;
    } else if (nchar == 7) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 7; i++)s[i] = s[i + 3];
        }
        s[7] = 0;
    } else if (nchar == 6) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 6; i++)s[i] = s[i + 4];
        }
        s[6] = 0;
    } else if (nchar == 5) {
        if (flags & ALIGNDX) {
            for (i = 0; i < 5; i++)s[i] = s[i + 5];
        }
        s[5] = 0;
    }
    if (flags & KG) {
        for (i = nchar; i > nchar-3; i--)s[i] = s[i -1];
        s[nchar-3]='.';
        s[nchar+1]=0;
    }


    if (flags & TRIMZERO) {
        for (i = 0; i < (nchar - 1); i++) {
            if (s[i] != '0')break;
        }
        for (temp = 0; i < nchar; i++) {
            s[temp] = s[i];
            temp++;
        }
        //curidx=temp;
        for (; temp < nchar; temp++) {
            s[temp] = ' ';
        }
    }
    if (flags & NOZERO) {
        if (flags & KG) {
            if (s[0] == '0')s[0] = ' ';
        }else{
            for (i = 0; i < (nchar - 1); i++) {
                if (s[i] == '0')s[i] = ' ';
                else break;
            }
        }
    }
    if (f & NEGATIVO)s[0] = '-';
    //	if(flags&TOPRN){
    ////		for(i=0;i<7;i++)tempbuf[i]=s[i];
    ////		sendPrn(s);
    //}
    //	else printlcd(add,s);
    printlcd(add, s);
}
*/
//**************************************************************
//***  Print per diagnosi **************************************
//**************************************************************

//void printD(byte add,word w){
//	char s[5];
//
//
//	s[0]=(char)(w/100)+'0';			//converte in digit
//	w=w%100;
//	s[1]=(char)(w/10)+'0';
//	w=w%10;
//	s[2]='.';
//	s[3]=(w%10)+'0';
//	if(s[0]=='0'){
//		s[0]=' ';
//		}
//	s[4]=0;
//	printlcd(add,s);
//}


//**************************************************************
//***  Print LCD  **********************************************
//************************************************************** 
void printlcd(byte add,char * p){
	byte i;
//
//	flags1&=(~FIOINT);
	waitbflcd();
	writelcd(0,0x80|add);
	for(i=0;*(p+i)!=0;i++){
		if(i>=20)break;
		waitbflcd();
		writelcd(1,*(p+i));
		}
	waitbflcd();
//	flags1|=FIOINT;
}

//**************************************************************
//***  Print LCD  **********************************************
//**************************************************************
/*
void printCharLcd(byte add,char c) {
    waitbflcd();
    writelcd(0, 0x80 | add);
    waitbflcd();
    writelcd(1, c);
    waitbflcd();
}
*/


//****************************************************
//***  write lcd  ************************************
//**************************************************** 
void writelcd(byte flcd,byte data){

    LCDRW_OFF();
    LCDBUS_OUT();
    if(flcd&FLCDRS)LCDRS_ON();else LCDRS_OFF();
    PORTWrite(LCD_PORTID,data);
    LCDEN_ON();
    SysDelayUs(2);
    LCDEN_OFF();
    SysDelayUs(2);
}



//**************************************************************
//***  clr lcd  ************************************************
//**************************************************************
void clrlcd(void){
	waitbflcd();
	writelcd(0,0x01);	//clear disp
	waitbflcd();
}

//**************************************************************
//***  init lcd  ***********************************************
//************************************************************** 
void initlcd(void){
	//flags1=flags1&(~FIOINT);
	
        SysDelayUs(40000 );
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	waitbflcd();
	writelcd(0,0x38);
	waitbflcd();
	writelcd(0,0x0c);	//display off
	waitbflcd();
	writelcd(0,0x01);	//clear disp
	waitbflcd();
	writelcd(0,0x06); 	//incremento
	waitbflcd();
	writelcd(0,0x14);	//no shift
	waitbflcd();
	writelcd(0,0x0c);	//disp on
	//flags1|=FIOINT;
} 

