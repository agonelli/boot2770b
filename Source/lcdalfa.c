

#include "global.h"
#include "hwtimer.h"
#define _DECL_LCDALFA
#include "lcdalfa.h"
#include "Utils\RTCUtil.h"

#ifndef ONLY_BASIC_FUNCTIONS
#include "hw_flash.h"
#include "printer.h"
#endif
//***** flcd ******
#define FLCDRS		0x01


//

//****************************************************
//***  waitbflcd  ************************************
//****************************************************
void waitbflcd(void){
    LCDBUS_IN();
    LCDRS_OFF();
    LCDRW_ON();
    SysDelayUs(10);
    for(;;){
	SysDelayUs(4);
	LCDEN_ON();
	SysDelayUs(4);
        if(LCDNOTBUSY())break;
  	LCDEN_OFF();
    }
    LCDEN_OFF();
    LCDBUS_OUT();
    SysDelayUs(4);
}

//**************************************************************
//***  setta cursore  ******************************************
//************************************************************** 
void setCursor(byte add,byte flags){
	flags1&=(~FIOINT);
	waitbflcd();
	writelcd(0,0x80|add);
	waitbflcd();
	writelcd(0,0x8|flags);
	waitbflcd();
	flags1|=FIOINT;
	
}

const char tohex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
//**************************************************************
//***  Print hexLCD ******************************************
//**************************************************************
void printwlcdhex(byte add,word w,byte nchar) {
    char s[5];

    s[0]=tohex[w>>12];
    s[1]=tohex[(w>>8)&0xf];
    s[2]=tohex[(w>>4)&0xf];
    s[3]=tohex[w&0x0f];

    if(nchar==1){
        s[0]=s[3];
    }else if(nchar==2){
        s[0]=s[2];
        s[1]=s[3];
    }else if(nchar==3){
        s[0]=s[1];
        s[1]=s[2];
        s[2]=s[3];
    }
    s[nchar]=0;
    printlcd(add,s);
}

//**************************************************************
//***  Print word LCD ******************************************
//**************************************************************
void FillStrw(char * buf, word w, byte nchar, byte flags){
	byte f=0;
	byte i;
	dword dw;

	if(w&0x8000){
		if(flags&NEGATIVO){
			f|=NEGATIVO;
			w=0xffff-w+1;
			}
		}
	dw=(dword)w;//+((dword)(flags&3)<<16);


	if((udm!=0)&&(flags&GR)){
		dw=dw*10000;
		dw=dw/28350;
		nchar--;
		}

	if((udm!=0)&&(flags&KG)){
		dw=dw*22046;
		dw=dw/10000;
		dw=approx10(dw);
		}
	buf[5]=0;
	buf[6]=0;
	buf[0]=(char)(dw/10000)+'0';			//converte in digit
	dw=dw%10000;
	buf[1]=(char)(dw/1000)+'0';
	dw=dw%1000;
	buf[2]=(char)(dw/100)+'0';
	dw=dw%100;
	buf[3]=(char)(dw/10)+'0';
	buf[4]=(dw%10+'0');

	if(nchar==4){
		if(flags&ALIGNDX){for(i=0;i<4;i++)buf[i]=buf[i+1];}
		buf[4]=0;
		}
	else if(nchar==3){
		if(flags&ALIGNDX){for(i=0;i<3;i++)buf[i]=buf[i+2];}
		buf[3]=0;
		}
	else if(nchar==2){
		if(flags&ALIGNDX){for(i=0;i<2;i++)buf[i]=buf[i+3];}
		buf[2]=0;
		}
    else if(nchar==1){
		if(flags&ALIGNDX){for(i=0;i<1;i++)buf[i]=buf[i+4];}
		buf[1]=0;
		}
//	if(flags&TRIMZERO){
//		for(i=0;i<(nchar-1);i++){
//			if(s[i]!='0')break;
//			}
//		}
	if(flags&NOZERO){
		for(i=0;i<(nchar-1);i++){
			if(buf[i]=='0')buf[i]=' ';else break;
			}
		}
	if(f&NEGATIVO)buf[0]='-';
	if(flags&KG){
		buf[5]=buf[4];
		buf[4]=buf[3];
		buf[3]=buf[2];
		buf[2]='.';
		if(buf[5]==' ')buf[5]='0';
		if(buf[4]==' ')buf[4]='0';
		if(buf[3]==' ')buf[3]='0';
		if(buf[1]==' ')buf[1]='0';
		}
	else if((udm!=0)&&(flags&GR)){
		buf[4]=0;
		buf[3]=buf[2];
		buf[2]='.';
		if(buf[1]==' ')buf[1]='0';
		}
	//if((flags&TOPRN)==0)printlcd(add,buf);

}

void printwlcd(byte add,word w,byte nchar,byte flags){
	FillStrw(gpbuf,w, nchar, flags);
	if((flags&TOPRN)==0)printlcd(add,gpbuf);
}

//**************************************************************
//***  Print word LCD ******************************************
//**************************************************************

void FillStrdw(char * buf, long dw, byte nchar, byte flags) {
	byte f=0;
	byte i;

	if(dw<0){
		if(flags&NEGATIVO){
			f|=NEGATIVO;
			dw=dw*(-1);
			}
		}

	if((udm!=0)&&(flags&KG)){
		dw=dw*22046;
		dw=dw/10000;
		dw=approx10(dw);
		}

	buf[10]=0;
	buf[0]=(char)(dw/1000000000)+'0';			//converte in digit
	dw=dw%1000000000;
	buf[1]=(char)(dw/100000000)+'0';
	dw=dw%100000000;
	buf[2]=(char)(dw/10000000)+'0';
	dw=dw%10000000;
	buf[3]=(char)(dw/1000000)+'0';
	dw=dw%1000000;
	buf[4]=(char)(dw/100000)+'0';
	dw=dw%100000;
	buf[5]=(char)(dw/10000)+'0';
	dw=dw%10000;
	buf[6]=(char)(dw/1000)+'0';
	dw=dw%1000;
	buf[7]=(char)(dw/100)+'0';
	dw=dw%100;
	buf[8]=(char)(dw/10)+'0';
	buf[9]=(dw%10+'0');
	if(nchar==5){
		if(flags&ALIGNDX){for(i=0;i<5;i++)buf[i]=buf[i+5];}
		buf[5]=0;
		}
        else if(nchar==6){
		if(flags&ALIGNDX){for(i=0;i<6;i++)buf[i]=buf[i+4];}
		buf[6]=0;
		}
	else if(nchar==8){
		if(flags&ALIGNDX){for(i=0;i<8;i++)buf[i]=buf[i+2];}
		buf[8]=0;
		}

	if(flags&NOZERO){
		for(i=0;i<(nchar-1);i++){
			if(buf[i]=='0')buf[i]=' ';else break;
			}
		}
	if(f&NEGATIVO)buf[0]='-';
	if(flags&KG){
		buf[5]=buf[4];
		buf[4]=buf[3];
		buf[3]=buf[2];
		buf[2]='.';
		if(buf[5]==' ')buf[5]='0';
		if(buf[4]==' ')buf[4]='0';
		if(buf[3]==' ')buf[3]='0';
		if(buf[1]==' ')buf[1]='0';
		buf[6]=0;
		}
}


void printdwlcd(byte add,long dw,byte nchar,byte flags){
    FillStrdw(gpbuf,dw,nchar,flags);

	if(flags&TOPRN){
	//	for(i=0;i<11;i++)gpbuf[i]=gpbuf[i];
		}
	else printlcd(add,gpbuf);
}

//**************************************************************
//***  Print dword LCD *****************************************
//************************************************************** 
//void printdwlcd(byte add,dword dw,byte nchar,byte flags) {
//    char s[11];
//    byte f = 0;
//    byte i;
//   // byte temp;
//
//    if (dw & 0x80000000) {
//        if (flags & NEGATIVO) {
//            f |= NEGATIVO;
//            dw = 0xffffffff - dw + 1;
//        }
//    }
//    for (i = 0; i < 11; i++)s[i] = 0;
//
//    s[0] = (char) (dw / 1000000000) + '0'; //converte in digit
//    dw = dw % 1000000000;
//    s[1] = (char) (dw / 100000000) + '0';
//    dw = dw % 100000000;
//    s[2] = (char) (dw / 10000000) + '0';
//    dw = dw % 10000000;
//    s[3] = (char) (dw / 1000000) + '0';
//    dw = dw % 1000000;
//    s[4] = (char) (dw / 100000) + '0';
//    dw = dw % 100000;
//    s[5] = (char) (dw / 10000) + '0';
//    dw = dw % 10000;
//    s[6] = (char) (dw / 1000) + '0';
//    dw = dw % 1000;
//    s[7] = (char) (dw / 100) + '0';
//    dw = dw % 100;
//    s[8] = (char) (dw / 10) + '0';
//    s[9] = (dw % 10 + '0');
//
//    if (nchar == 9) {
//        if (flags & ALIGNDX) {
//            for (i = 0; i < 9; i++)s[i] = s[i + 1];
//        }
//        s[9] = 0;
//    } else if (nchar == 8) {
//        if (flags & ALIGNDX) {
//            for (i = 0; i < 8; i++)s[i] = s[i + 2];
//        }
//        s[8] = 0;
//    } else if (nchar == 7) {
//        if (flags & ALIGNDX) {
//            for (i = 0; i < 7; i++)s[i] = s[i + 3];
//        }
//        s[7] = 0;
//    } else if (nchar == 6) {
//        if (flags & ALIGNDX) {
//            for (i = 0; i < 6; i++)s[i] = s[i + 4];
//        }
//        s[6] = 0;
//    } else if (nchar == 5) {
//        if (flags & ALIGNDX) {
//            for (i = 0; i < 5; i++)s[i] = s[i + 5];
//        }
//        s[5] = 0;
//    }
//    if (flags & KG) {
//        for (i = nchar; i > nchar-3; i--)s[i] = s[i -1];
//        s[nchar-3]='.';
//        s[nchar+1]=0;
//    }
//
//
////    if (flags & TRIMZERO) {
////        for (i = 0; i < (nchar - 1); i++) {
////            if (s[i] != '0')break;
////        }
////        for (temp = 0; i < nchar; i++) {
////            s[temp] = s[i];
////            temp++;
////        }
////        //curidx=temp;
////        for (; temp < nchar; temp++) {
////            s[temp] = ' ';
////        }
////    }
//    if (flags & NOZERO) {
//        if (flags & KG) {
//            if (s[0] == '0')s[0] = ' ';
//        }else{
//            for (i = 0; i < (nchar - 1); i++) {
//                if (s[i] == '0')s[i] = ' ';
//                else break;
//            }
//        }
//    }
//    if (f & NEGATIVO)s[0] = '-';
//    //	if(flags&TOPRN){
//    ////		for(i=0;i<7;i++)tempbuf[i]=s[i];
//    ////		sendPrn(s);
//    //}
//    //	else printlcd(add,s);
//    printlcd(add, s);
//}
//
//**************************************************************
//***  Print per diagnosi **************************************
//************************************************************** 
//void printD(byte add,word w){
//	char s[5];
//
//
//	s[0]=(char)(w/100)+'0';			//converte in digit
//	w=w%100;
//	s[1]=(char)(w/10)+'0';
//	w=w%10;
//	s[2]='.';
//	s[3]=(w%10)+'0';
//	if(s[0]=='0'){
//		s[0]=' ';
//		}
//	s[4]=0;
//	printlcd(add,s);
//}


//**************************************************************
//***  Print LCD  **********************************************
//************************************************************** 
void printlcd(byte add,char * p){
	byte i;
//
	flags1&=(~FIOINT);
	waitbflcd();
	writelcd(0,0x80|add);
	for(i=0;*(p+i)!=0;i++){
		if(i>=20)break;
		waitbflcd();
		writelcd(1,*(p+i));
		}
	waitbflcd();
	flags1|=FIOINT;
}

//**************************************************************
//***  Print LCD  **********************************************
//**************************************************************
void printCharLcd(byte add,char c) {
	flags1&=(~FIOINT);
    waitbflcd();
    writelcd(0, 0x80 | add);
    waitbflcd();
    writelcd(1, c);
    waitbflcd();
	flags1|=FIOINT;
}



//****************************************************
//***  write lcd  ************************************
//**************************************************** 
void writelcd(byte flcd,byte data){

    LCDRW_OFF();
    LCDBUS_OUT();
    if(flcd&FLCDRS)LCDRS_ON();else LCDRS_OFF();
    PORTWrite(LCD_PORTID,data);
    SysDelayUs(10);
    LCDEN_ON();
    SysDelayUs(4);
    LCDEN_OFF();
    SysDelayUs(4);
}



//**************************************************************
//***  clr lcd  ************************************************
//**************************************************************
void clrlcd(void){
	flags1&=(~FIOINT);
	waitbflcd();
	writelcd(0,0x01);	//clear disp
	waitbflcd();
	flags1|=FIOINT;
}

//**************************************************************
//***  init lcd  ***********************************************
//************************************************************** 
void initlcd(void){
	flags1=flags1&(~FIOINT);
	
    SysDelayUs(40000 );
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	writelcd(0,0x38);
	SysDelayUs(10000 );//attesa 10mS
	waitbflcd();
	writelcd(0,0x38);
	waitbflcd();
	writelcd(0,0x0c);	//display off
	waitbflcd();
	writelcd(0,0x01);	//clear disp
	waitbflcd();
	writelcd(0,0x06); 	//incremento
	waitbflcd();
	writelcd(0,0x14);	//no shift
	waitbflcd();
	writelcd(0,0x0c);	//disp on
	flags1|=FIOINT;
} 


const unsigned char symboltab[8][8]={
    {0x0e,0x0a,0x1f,0x11,0x11,0x11,0x11,0x1f},
    {0x1f,0x04,0x0e,0x15,0x04,0x04,0x04,0x04},
    {0x04,0x04,0x04,0x04,0x15,0x0e,0x04,0x1f},
    {0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10},
    {0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18},
    {0x1c,0x1c,0x1c,0x1c,0x1c,0x1c,0x1c,0x1c},
    {0x1e,0x1e,0x1e,0x1e,0x1e,0x1e,0x1e,0x1e},
    {0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f},
};

//**************************************************************
//***  Set Symbol  *********************************************
//**************************************************************
void setSymbolLCD(void) {
    byte i;
    byte j;
	flags1=flags1&(~FIOINT);

    waitbflcd();
    writelcd(0, 0x40);
    waitbflcd();
    for (j = 0; j < 8; j++) {
        for (i = 0; i < 8; i++) {
            waitbflcd();
            writelcd(1, symboltab[j][i]);
        }
    }
	flags1|=FIOINT;
}

#ifndef ONLY_BASIC_FUNCTIONS
//****************************************
//***  stringa da Flash a lcd ***********
//****************************************
//void FillStrFlash(char * buf, word flashadd, byte flag) {
//
//    byte nc;
//    byte i;
//    byte f=0;
//#ifndef _2770B    
//    if((flashadd>=135)&&((flag & ABSADD) == 0))f=1;
//    if (flashadd == GASNAME) {
//        nc = LEN_strGAS;
//        for(i=0;i<LEN_strGAS;i++){
//            buf[i]=strGAS[i];
//        }
//    } else {
//        if ((flag & ABSADD) == 0) {
//            nc = nchar[flashadd];
//            if(flashadd>=135)flashadd-=135;
//            flashadd = (flashadd * (30 * 16))+((word) lingua * 16);
//        } else
//            nc = 16;
//        if(f==0){
//            SFLASH_RdData( (dword)flashadd + SFADD_LINGUA, (byte*)buf, nc);
//        }else{
//            SFLASH_RdData( (dword)flashadd + SFADD_LINGUA2, (byte*)buf, nc);
//        }
//    }
//    buf[nc] = 0;
//#else
//    nc = 20;
//    SFLASH_RdData( (dword)flashadd + SFADD_LINGUA, (byte*)buf, nc);
//    buf[nc] = 0;
//#endif
//}

void printFlashLCD(word flashadd,byte lcdadd,byte nchar,byte lingua){
    byte buf[64];
    dword fadd;
    dword offset;
    if(flashadd<300){
        offset=0;
        }else{
        offset=0x10000;
        flashadd-=300;        
        }

    fadd=(dword)flashadd*200+((word)(lingua%10)*20);
    fadd+=(0x20000*(lingua/10));
//     readFlash(c,fadd,nchar);
    //add=add*200+((word)lingua*20);
    //readFlash(c,(dword)add,nchar);
    SFLASH_RdData( (dword)fadd+offset , buf, nchar);
    buf[nchar]=0;
//	FillStrFlash(gpbuf, flashadd, flag);
	printlcd(lcdadd,(char*)buf);
}

#endif
////****************************************
////***  stringa da flash a lcd ***********
////****************************************
//void printFlashLCD(word add,byte lcdadd,byte nchar,byte flag) {
//    byte i;
//    //	byte c=0;
//    byte buf[21];
//    dword dadd;
//
//    if ((flag & ABSADD) == 0) {
//        dadd = (dword) add;
//        dadd = dadd * 200;
//        dadd=dadd+((dword)lingua*20);
//
//        //		StartReadFlash(dadd,nchar);
//        //		ReadFlash(buf,20);
//    }else dadd=add;
//    (void) SFLASH_RdData(dadd, buf, 20);
//    //	flags1&=(~FIOINT);
//    waitbflcd();
//    writelcd(0, 0x80 | lcdadd);
//    for (i = 0; i < nchar; i++) {
//        waitbflcd();
//        writelcd(1, buf[i]);
//    }
//    //	flags1|=FIOINT;
//}


//const unsigned char UpSymbol[]={0x04,0x0e,0x1f,0x0,0x0,0,0,0};
//const unsigned char DownSymbol[]={0x0,0x0,0x0,0x0,0x0,0x1f,0x0e,0x04};
//#define NSTEPBARV   24
//
//void barV(unsigned char val, unsigned char max) {
//    char s[2] = {0, 0};
//    unsigned char buf[32];
//    unsigned char i;
//    unsigned char step;
//    unsigned short w;
//
//    if(max>NSTEPBARV)step=1;else step=NSTEPBARV/max;
//
//    for(i=0;i<8;i++){
//        buf[i]=UpSymbol[i];
//        buf[i+8]=0;
//        buf[i+16]=0;
//        buf[i+24]=DownSymbol[i];
//    }
//    w=val;
//    w=(w*NSTEPBARV)/max;
//
//    for(i=0;i<step;i++){
//        buf[4+w+i]=0x1f;
//    }
////    buf[4+w]=0x1f;
////    if(step>=2){
////        if(w<NSTEPBARV-1)buf[4+1+w]=0x1f;else buf[4-1+w]=0x1f;
////    }
////    if(step>=3){
////        if(w>0)buf[4-1+w]=0x1f;else buf[4+1+w]=0x1f;
////    }
//    waitbflcd();
//    writelcd(0, 0x40);
//    waitbflcd();
//    for (i = 0; i < 32; i++) {
//        waitbflcd();
//        //			waitbflcd();
//        writelcd(1, buf[i]);
//    }
//    waitbflcd();
//
//    printCharLcd(RIGA1 + 19, 0);
//    printCharLcd(RIGA2 + 19, 1);
//    printCharLcd(RIGA3 + 19, 2);
//    printCharLcd(RIGA4 + 19, 3);
//}

//const unsigned char bartab[]={0x00,0x10,0x18,0x1c,0x1e};
//
//void barH(unsigned char add, long min, long max, long val) {
//    long w;
//    byte i;
//    byte v;
//    byte p;
//    unsigned char buf[40];
//
//    if (val > max)val = max;
//    if (val < min)val = min;
//
//    w = (dword) val;
//    w = w * (dword) 70;
//    if ((max - min) > 1)w = w / (dword) (max - min);
//    else w = 0;
//    p = (byte) (w % 5);
//    v = (byte) (w / 5);
//    //**************************************************
//    for (i = 0; i < 8; i++)buf[i] = 0xff;
//    if (v == 0) { //primo carattere 0
//        for (i = 1; i < 7; i++)buf[i] = bartab[p] | 0x10;
//    }
//    for (i = 8; i < 16; i++)buf[i] = 0xff; //carattere pieno 1
//
//    for (i = 16; i < 24; i++) { //carattere parzializzato 2
//        if ((i == 16) || (i == 23))buf[i] = 0xff;
//        else buf[i] = bartab[p];
//
//    }
//    for (i = 24; i < 32; i++) { //carattere vuoto 3
//        if ((i == 24) || (i == 31))buf[i] = 0xff;
//        else buf[i] = 0;
//    }
//
//    for (i = 32; i < 40; i++)buf[i] = 0xff;
//    for (i = 33; i < 39; i++) { //ultimo carattere 4
//        if (v == 13)buf[i] = bartab[p] | 0x01;
//        else buf[i] = 0x01;
//    }
//    waitbflcd();
//    writelcd(0, 0x40);
//    waitbflcd();
//    for (i = 0; i < 40; i++) {
//        waitbflcd();
//        writelcd(1, buf[i]);
//    }
//    waitbflcd();
//    //**************************************************
//    printCharLcd(add + 2, 0); //primo carattere
//    i = 1;
//    if (v > 1) {
//        for (i = 1; i < v; i++)printCharLcd(add + i + 2, 1);
//    }
//    if (p) {
//        printCharLcd(add + i + 2, 2);
//    }
//    for (i = v + 1; i < 13; i++)printCharLcd(add + i + 2, 3);
//    printCharLcd(add + 2 + 13, 4); //ultimo carattere
//    printlcd(add, "0%");
//    printlcd(add + 16, "100%");
//    //if ((v == 0) && (p == 0))printCharLcd(add + i, ' ');
//}

//**************************************************************
//***  barra di scorrimento ************************************
//**************************************************************
void bar(byte add, word min, word max, word val, byte flags) {
    dword w;
    byte i;
    byte v;
    byte p;

    if (val > max)val = max;
    if (val < min)val = min;
    w = (dword) val;
    if (flags & NOPERC)w = w * (dword) 100; //80=16*5
    else w = w * (dword) 65; //65=13*5
    w = w / (dword) (max - min);
    val = (word) (w & 0xffff);
    p = (byte) (val % 5);
    v = (byte) (val / 5);
    i = 0;
    if (v) {
        for (; i < v; i++)
            printCharLcd(add + i, CUSTSIMB_5_5);
    }
    if (p) {
        printCharLcd(add + i, CUSTSIMB_5_1 + p - 1);
    }
    if ((flags & NOPERC) == 0) {
        for (i = v + 1; i < 13; i++)printCharLcd(add + i, ' ');
        w = (dword) val;
        w = w * 100;
        w = w / 65;
        printwlcd(add + 13, (word) w, 2, ALIGNDX | NOZERO);
        printlcd(add + 15, "%");
    } else {
        for (i = v + 1; i < 20; i++)printCharLcd(add + i, ' ');
    }
    if(i>19)i=19;
    if ((v == 0) && (p == 0))printCharLcd(add + i, ' ');
}


//**************************************************************
//***  approssimazione 10***************************************
//**************************************************************
dword approx10(dword dw){
	dword temp;
	temp=(dw%10);
	if(temp>=0){
		if(temp>5)dw=dw+(dword)(10-temp);
		else dw=dw-temp;
		}
	else{
		if(temp>=-5)dw=dw-(dword)(10+temp);
		else dw=dw-temp;
		}
	return dw;
}

#ifndef ONLY_BASIC_FUNCTIONS
//**************************************************************
//***  PrintTimeLCD ********************************************
//**************************************************************
void FillStrTime(char * buf){
	FillStrw(buf,(word)RTC.RTC_Hour,2,ALIGNDX);
	FillStrw(&buf[3],(word)RTC.RTC_Min,2,ALIGNDX);
	buf[2]=':';
	buf[5]=0;

}

void printTimeLCD(byte add){
	FillStrTime(gpbuf);
	printlcd(add,gpbuf);
}

//**************************************************************
//***  PrintDateLCD ********************************************
//**************************************************************
void FillStrDate(char * buf, byte flags){
	FillStrw(buf,(word)RTC.RTC_Mday,2,ALIGNDX);
	FillStrw(&buf[3],(word)RTC.RTC_Mon,2,ALIGNDX);
	if(flags&ENTIREYEAR) {
		FillStrw(&buf[8],(word)RTC.RTC_Year,2,ALIGNDX);
	} else {
		FillStrw(&buf[6],(word)RTC.RTC_Year,2,ALIGNDX);
	}

	if(flags&ENTIREYEAR) {
		buf[6] = '2';
		buf[7] = '0';
		buf[10] = 0;
	} else {
		buf[8] = 0;
	}
	buf[2]='/';
	buf[5] = '/';
}

void printDateLCD(byte add, byte flags){
	FillStrDate(gpbuf,flags);
	printlcd(add,gpbuf);

}
#endif

void printBuflcd(byte add,const char * p, byte len){
	byte i;

	waitbflcd();
	writelcd(0,0x80|add);
	for(i=0;i<len;i++){
		waitbflcd();
		writelcd(1,(byte)p[i]);
		}
}

#ifndef ONLY_BASIC_FUNCTIONS

//****************************************
//***  stringa da eeprom *****************
//****************************************
void printFlashsws(word flashadd1,word w,word flashadd2){
	 byte l;

    FillStrFlash(gpbuf, flashadd1, LANGADD);
	l=nchar[flashadd1];
	if(flashadd2==(STRKG+udm)){
		FillStrw(&gpbuf[l],w,5,NOZERO|NEGATIVO|KG|TOPRN);
		l += 6;
	} else if(flashadd2==(STRGR+udm)){
		FillStrw(&gpbuf[l],w,4,ALIGNDX|NOZERO|NEGATIVO|GR|TOPRN);
		l += 4;
	} else {
		FillStrw(&gpbuf[l],w,5,NOZERO|NEGATIVO|TOPRN);
		l += 5;
	}

	if(flashadd2==STRPRIMI){
		gpbuf[l]=39;	//'
		l++;
		}
	else if(flashadd2!=0){
        FillStrFlash(&gpbuf[l], flashadd2, LANGADD);
		l+=nchar[flashadd2];
		}

	//for(i=0;i<l;i++)if(gpbuf[i]<' ')gpbuf[i]=' ';
	gpbuf[l]=0;
	print((byte*)gpbuf,0);
}

//****************************************
//***  stringa da eeprom a lcd ***********
//****************************************
void printFlashsc(word flashadd,byte * c){
    byte i;
    byte l;
    byte b[24];

    FillStrFlash((char*) b, flashadd, LANGADD);
    l = nchar[flashadd];
    for (i = 0; i < (24 - nchar[flashadd]); i++) {
        if (c[i] != 0) {
            b[l] = c[i];
            l++;
        } else break;
    }

    b[l] = 0;
    print(b, 0);
}

#endif
//****************************************
//***  stringa da eeprom a lcd ***********
//****************************************

//void printFlashDbLCD(dword flashadd,byte lcdadd,byte nc){
//	SFLASH_RdData(flashadd, (byte*)gpbuf, nc);
//	gpbuf[nc]=0;
//	printlcd(lcdadd,gpbuf);
//}
