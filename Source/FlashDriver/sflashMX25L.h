#ifndef _SFMX25L_16_32_64_H
#define _SFMX25L_16_32_64_H
//#include "inc/hw_types.h"
//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

#define ID_MX25L6405D 0xC215
#define SFMX25L_16_32_64_MaxSizeBufferWrite 256

#define SFMX25L_32_SectorSize 4096
#define SFMX25L_32_BlockSize 0x10000

#define SFMX25L_16_Size 2097152
#define SFMX25L_32_Size 4194304
#define SFMX25L_64_Size 8388608

unsigned long SFMX25L_16_32_64_GetId(unsigned char mem);
unsigned char SFMX25L_16_32_64_ChipErase(unsigned char mem);
unsigned char SFMX25L_16_32_64_SectorErase(unsigned long addr,unsigned char mem);
unsigned short SFMX25L_16_32_64_WrData(unsigned long addr, void* datax, unsigned short len,unsigned char mem);
unsigned short SFMX25L_16_32_64_RdData(unsigned long addr, void* datax, unsigned short len, unsigned char mem);
unsigned short SFMX25L_16_32_64_RdData_ENH(unsigned long addr, unsigned char* datax, unsigned long len, unsigned char mem);
unsigned char SFMX25L_16_32_64_RdStato(unsigned char mem);
unsigned char SFMX25L_16_32_64_ResetProt(unsigned char mem);
unsigned char SFMX25L_16_32_64_WaitEndWr(unsigned char mem);
void SFMX25L_16_32_64_WrEnable(unsigned char mem);
void SFMX25L_16_32_64_WrDisable(unsigned char mem);
void SFMX25L_16_32_64_WrStato(unsigned char stato, unsigned char mem);
unsigned char SFMX25L_16_32_64_BlockErase(unsigned long addr, unsigned char mem);

extern void WrByte(unsigned char val);
extern unsigned char RdByte(void);
extern void RdBytes(unsigned char * datax, unsigned long len);
extern void SFMX25L_16_32_64_SelectFlash(unsigned char mem);
extern void SFMX25L_16_32_64_DeselectFlash(unsigned char mem);


//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif


#endif
