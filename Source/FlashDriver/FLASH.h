/* 
 * File:   FLASH.h
 * Author: matteob
 *
 * Created on 10 giugno 2014, 10.14
 */

#ifndef FLASH_H
#define	FLASH_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef struct {
        unsigned short usID;
        long lSize;
        long lSectorSize;
        long lBlockSize;
        unsigned short usMaxSizeBufferWrite;

        unsigned long (*pfnGetId)(unsigned char mem);
        unsigned char (*pfnChipErase)(unsigned char mem);
        unsigned char (*pfnSectorErase)(unsigned long addr, unsigned char mem);
        unsigned short (*pfnWrData)(unsigned long addr, void* data, unsigned short len, unsigned char mem);
        unsigned short (*pfnRdData)(unsigned long addr, void* data, unsigned short len, unsigned char mem);
        unsigned char (*pfnBlockErase)(unsigned long addr, unsigned char mem);
    }
    tFLASH;


#ifdef	__cplusplus
}
#endif

#endif	/* FLASH_H */

