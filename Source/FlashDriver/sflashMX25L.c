//08.04.10 Andrea
//derivato da sflash di Ballanti e adattato a micro Texas

//*** Tipi di Dato




#include "sflashMX25L.h"



//===========================================================================
//Descrizione:	Legge ID del dispositivo
//Parametri:		Nessuno
//Ritorno:		ID	bit15 - bit8	manufacturer id	(0xc2)
//					bit7  - bit0	device id		(0x13 = MX25L8005  0x15 = MX25L3205)
//===========================================================================

//flash in megabit
//usare solo le prime 4 funzioni
unsigned long SFMX25L_16_32_64_GetId(unsigned char mem){
	unsigned long id;

	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x90);		// comando
	WrByte(0x0);		// comando
	WrByte(0x0);		// comando
	WrByte(0x0);		// comando
	
	id = 0;
 	id =RdByte();
 	id = id << 8;
	id |= RdByte();
	SFMX25L_16_32_64_DeselectFlash(mem);
	return(id);
}

//===========================================================================
//Descrizione:	Cancella tutta la flash seriale
//Parametri:		nessuno
//Ritorno:		errore di cancellazione 
//					0 --> ok
//					1 --> errore
//===========================================================================
unsigned char SFMX25L_16_32_64_ChipErase(unsigned char mem){
	// verifica protezione
	if (SFMX25L_16_32_64_ResetProt(mem))return(1);
	(void)SFMX25L_16_32_64_WaitEndWr(mem);
	// abilita scrittura
	SFMX25L_16_32_64_WrEnable(mem);
	(void)SFMX25L_16_32_64_WaitEndWr(mem);
	SFMX25L_16_32_64_SelectFlash(mem);		// invia comando cancella chip
	WrByte(0x60);		// comando
//	WrByte(0xC7);		// comando
	SFMX25L_16_32_64_DeselectFlash(mem);
	(void)SFMX25L_16_32_64_WaitEndWr(mem);
  	SFMX25L_16_32_64_WrDisable(mem);
	
	// attesa cancellazione
	if (SFMX25L_16_32_64_WaitEndWr(mem))return(1);
	return(0);
}

//===========================================================================
//Descrizione:	Cancella un settore della flash seriale
//Parametri:		settore	= settore da cancellare
//Ritorno:		errore di cancellazione 
//					0 --> ok
//					1 --> errore
//===========================================================================
unsigned char SFMX25L_16_32_64_SectorErase(unsigned long addr, unsigned char mem){
	unsigned char ad1,ad2,ad3;


	if (SFMX25L_16_32_64_ResetProt(mem))return(1);	// verifica protezione
	SFMX25L_16_32_64_WrEnable(mem);					// abilita scrittura
	ad1 = (unsigned char)((addr >> 16) & 0xff);
	ad2 = (unsigned char)((addr >> 8) & 0xff);
	ad3 = (unsigned char)(addr & 0xff);
	// invia comando cancella settore
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x20);		// comando
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
	SFMX25L_16_32_64_DeselectFlash(mem);

	if (SFMX25L_16_32_64_WaitEndWr(mem))return(1);	// attesa cancellazione
	return(0);
}

//===========================================================================
//Descrizione:	Cancella un blocco della flash seriale
//Parametri:		settore	= settore da cancellare
//Ritorno:		errore di cancellazione 
//					0 --> ok
//					1 --> errore
//===========================================================================
unsigned char SFMX25L_16_32_64_BlockErase(unsigned long addr, unsigned char mem){
	unsigned char ad1,ad2,ad3;

	if (SFMX25L_16_32_64_ResetProt(mem))return(1);	// verifica protezione
	SFMX25L_16_32_64_WrEnable(mem);					// abilita scrittura
	ad1 = (unsigned char)((addr >> 16) & 0xff);
	ad2 = (unsigned char)((addr >> 8) & 0xff);
	ad3 = (unsigned char)(addr & 0xff);
	// invia comando cancella settore
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0xD8);		// comando
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
	SFMX25L_16_32_64_DeselectFlash(mem);

	if (SFMX25L_16_32_64_WaitEndWr(mem))return(1);	// attesa cancellazione
	return(0);
}

//===========================================================================
//Descrizione:	Scrive un buffer nella flash seriale
//				La scrittura termina alla fine della pagina
//Parametri:		addr	= indirizzo flash
//				data	= puntatore ai dati
//				len		= numero unsigned char
//Ritorno:		numero unsigned char scritti
//===========================================================================
 unsigned short SFMX25L_16_32_64_WrData(unsigned long addr, void* data, unsigned short len, unsigned char mem){
	unsigned char	ad1, ad2, ad3;
	unsigned short	cnt;
	 unsigned char * lpData;

	if (len == 0)return(0);
	if (SFMX25L_16_32_64_ResetProt(mem))return(0);	// verifica protezione
	SFMX25L_16_32_64_WrEnable(mem);		// abilita scrittura
	ad1 = (unsigned char)((addr >> 16) & 0xff);
	ad2 = (unsigned char)((addr >> 8) & 0xff);
	ad3 = (unsigned char)(addr & 0xff);
	// invia comando scrittura pagina
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x02);		// comando scrittura
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
	cnt = 0;
	 lpData = data;
	while (cnt < len) {
//		WrByte((*(unsigned char*)(data)));        
//        data += 1;
		WrByte(*(unsigned char*)(lpData)++);
		cnt++;
		ad3++;
		if (ad3 == 0)	// termina se fine pagina
			break;
	}
	SFMX25L_16_32_64_DeselectFlash(mem);
	if (SFMX25L_16_32_64_WaitEndWr(mem))	return(0);	// attesa scrittura
	return(cnt);
}

//===========================================================================
//Descrizione:	Legge un buffer dalla flash seriale
//				La lettura termina alla fine della pagina
//Parametri:		addr	= indirizzo flash
//				data	= puntatore ai dati
//				len		= numero unsigned char
//Ritorno:		numero unsigned char letti
//===========================================================================
unsigned short SFMX25L_16_32_64_RdData(unsigned long addr, void* data, unsigned short len, unsigned char mem){
	unsigned char	ad1, ad2, ad3;
	//unsigned short	cnt;


	if (len == 0)return(0);
	ad1 = (unsigned char)((addr >> 16) & 0xff);
	ad2 = (unsigned char)((addr >> 8) & 0xff);
	ad3 = (unsigned char)(addr & 0xff);
	// invia comando lettura
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x03);		// comando lettura
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);

	RdBytes((unsigned char*)(data), len);

	SFMX25L_16_32_64_DeselectFlash(mem);
	return(len);
}


unsigned short SFMX25L_16_32_64_RdData_ENH(unsigned long addr, unsigned char* data, unsigned long len, unsigned char mem){
	unsigned char	ad1, ad2, ad3;
	unsigned long	cnt;

	if (len == 0)return(0);
	
	ad1 = (unsigned char)((addr >> 16) & 0xff);
	ad2 = (unsigned char)((addr >> 8) & 0xff);
	ad3 = (unsigned char)(addr & 0xff);
	// invia comando lettura
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x03);		// comando lettura
	WrByte(ad1);		// indirizzo
	WrByte(ad2);
	WrByte(ad3);
	cnt = 0;
	while (cnt < len) {
		*data++ = RdByte();
		cnt++;
		ad3++;
		if (ad3 == 0)	// termina se fine pagina
			break;
	}
	SFMX25L_16_32_64_DeselectFlash(mem);
	return(cnt);
}



//===========================================================================
//Descrizione:	Disabilita protezione scrittura
//Parametri:		Nessuno
//Ritorno:		errore
//					0 --> ok
//					1 --> errore
//===========================================================================
unsigned char SFMX25L_16_32_64_ResetProt(unsigned char mem){
	unsigned char stato;

	stato = SFMX25L_16_32_64_RdStato(mem);
	if (stato & 0x9c) {
		SFMX25L_16_32_64_WrEnable(mem);
		SFMX25L_16_32_64_WrStato(0x00, mem);
		if (SFMX25L_16_32_64_WaitEndWr(mem))return(1);
		stato = SFMX25L_16_32_64_RdStato(mem);
		if (stato & 0x9c)return(1);
		}
	return(0);
}

//===========================================================================
//Descrizione:	Attesa fine scrittura
//				La funzione torna al termine della scrittura
//Parametri:		Nessuno
//Ritorno:		errore
//					0 --> ok
//					1 --> errore
//===========================================================================
unsigned char SFMX25L_16_32_64_WaitEndWr(unsigned char mem){
	unsigned char	stato = 1;
	unsigned char	error = 0;

	while ((stato & 0x1) > 0){
		SFMX25L_16_32_64_SelectFlash(mem);
		WrByte(0x05);		// comando lettura stato
		stato = RdByte();
		SFMX25L_16_32_64_DeselectFlash(mem);
	}
	return(error);
}

//===========================================================================
//Descrizione:	Abilita scrittura
//Parametri:		Nessuno
//Ritorno:		Nessuno
//===========================================================================
void SFMX25L_16_32_64_WrEnable(unsigned char mem){
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x06);		// comando
	SFMX25L_16_32_64_DeselectFlash(mem);
}

//===========================================================================
//Descrizione:	Disabilita scrittura
//Parametri:		Nessuno
//Ritorno:		Nessuno
//===========================================================================
void SFMX25L_16_32_64_WrDisable(unsigned char mem){
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x04);		// comando
	SFMX25L_16_32_64_DeselectFlash(mem);
}

//===========================================================================
//Descrizione:	Disabilita scrittura
//Parametri:		Nessuno
//Ritorno:		Nessuno
//===========================================================================
void SFMX25L_16_32_64_WrStato(unsigned char stato, unsigned char mem){
	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x01);		// comando
	WrByte(stato);		// stato
	SFMX25L_16_32_64_DeselectFlash(mem);
}


//===========================================================================
//Descrizione:	Legge il registro di stato del dispositivo
//Parametri:		Nessuno
//Ritorno:		unsigned char di stato
//===========================================================================
unsigned char SFMX25L_16_32_64_RdStato(unsigned char mem){
	unsigned char	stato;

	SFMX25L_16_32_64_SelectFlash(mem);
	WrByte(0x05);		// comando
	stato = RdByte();
	SFMX25L_16_32_64_DeselectFlash(mem);
	return(stato);
}


