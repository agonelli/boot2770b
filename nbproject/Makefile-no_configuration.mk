#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-no_configuration.mk)" "nbproject/Makefile-local-no_configuration.mk"
include nbproject/Makefile-local-no_configuration.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=no_configuration
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c "Source/MDD file System/FSIO.c" Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o "${OBJECTDIR}/Source/MDD file System/FSIO.o" ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d ${OBJECTDIR}/Source/HWInterface/SPI.o.d ${OBJECTDIR}/Source/HWInterface/HWUART.o.d ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" ${OBJECTDIR}/Source/NVMem.o.d ${OBJECTDIR}/Source/USB/usb_config.o.d ${OBJECTDIR}/Source/USB/usb_host.o.d ${OBJECTDIR}/Source/USB/usb_host_msd.o.d ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d ${OBJECTDIR}/Source/lcdalfa.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o ${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o

# Source Files
SOURCEFILES=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c Source/MDD file System/FSIO.c Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-no_configuration.mk ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F256H
MP_LINKER_FILE_OPTION=,--script="btl_32MX250F256H_generic.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  .generated_files/flags/no_configuration/32c0fb28597e158bfbe14cdf7b67c3dae7e27806 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  .generated_files/flags/no_configuration/92842a2fa8bd41425baa53943fe65300b6cbcffb .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  .generated_files/flags/no_configuration/16679b279f71b29a2b9a87e7031ed5a0a166bbb5 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  .generated_files/flags/no_configuration/87bdcbea40ed2a2357b65d68bc250096fbb569fc .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  .generated_files/flags/no_configuration/42dd485633c060da75ae881bd0f12c9f71cd4a07 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  .generated_files/flags/no_configuration/4c375e671c6fb69040460cc5e130f64cd7ca11b6 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  .generated_files/flags/no_configuration/381cb0864b0f8685434eeaad7ea14788dd64b9af .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/no_configuration/48649034c296df0c1421976e179a0d4a0433a56f .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  .generated_files/flags/no_configuration/ddf4e7b2cc0884152a1fdf2a75ca796519c977bf .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  .generated_files/flags/no_configuration/66f0f8944774c6fb53e25cac9481f55d63117670 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  .generated_files/flags/no_configuration/f51e97221a95f8f45f5b01d5da6879e1e282b62f .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  .generated_files/flags/no_configuration/f3ceedd6284689c333b5a4d9b1872be9a60ac7fc .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/no_configuration/a2d9d7a25de5716ed12543229bd319fa09228871 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  .generated_files/flags/no_configuration/ec8d5b54d651e231200463523728d9c865e967b7 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  .generated_files/flags/no_configuration/f2adb3af0b32bff9f8ea9c94036166fa906ca58c .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  .generated_files/flags/no_configuration/e3c73c12acdb97f5ecdff64926afb9a3fd05d2a .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  .generated_files/flags/no_configuration/f7c56ea21b6e9c2a4cde8e9aea545dc6d7939861 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  .generated_files/flags/no_configuration/3aa38c8e50d2c26964ec7367f666925e3204b94f .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  .generated_files/flags/no_configuration/9683e0c9709383320c23763220df2eef1024d5b6 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  .generated_files/flags/no_configuration/cd2a915b50f595107611714f7b25d0c81db2af1c .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  .generated_files/flags/no_configuration/41dc2816486dfdeb789321b8d1f85a56ba60fd8 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/no_configuration/f2f08f53e5a01ab21ecb798d8697d21b7f7d8ee1 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  .generated_files/flags/no_configuration/c075e55db834c7591eb36c14db6bf57325b3f1be .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  .generated_files/flags/no_configuration/4c801cebffd2d53a44c009177eab6629bab3717a .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  .generated_files/flags/no_configuration/9b4d864235cbeb47797e9d9b6b581ef875066c75 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  .generated_files/flags/no_configuration/b2543a83c195a831a658e842aa5548ede2651426 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/no_configuration/2cb96cb8d1a899e62b7e652c2cea81166e0c80d1 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  .generated_files/flags/no_configuration/d38cb87794003c3a5d800e4431ff34076188e8a1 .generated_files/flags/no_configuration/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)      -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	
else
${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	${MP_CC_DIR}\\xc32-bin2hex ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
