#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-debug.mk)" "nbproject/Makefile-local-debug.mk"
include nbproject/Makefile-local-debug.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=debug
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c "Source/MDD file System/FSIO.c" Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o "${OBJECTDIR}/Source/MDD file System/FSIO.o" ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d ${OBJECTDIR}/Source/HWInterface/SPI.o.d ${OBJECTDIR}/Source/HWInterface/HWUART.o.d ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" ${OBJECTDIR}/Source/NVMem.o.d ${OBJECTDIR}/Source/USB/usb_config.o.d ${OBJECTDIR}/Source/USB/usb_host.o.d ${OBJECTDIR}/Source/USB/usb_host_msd.o.d ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d ${OBJECTDIR}/Source/lcdalfa.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o ${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o

# Source Files
SOURCEFILES=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c Source/MDD file System/FSIO.c Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-debug.mk dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F256H
MP_LINKER_FILE_OPTION=,--script="btl_32MX250F256H_generic.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	@${FIXDEPS} "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/NVMem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/lcdalfa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	@${FIXDEPS} "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/NVMem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	@${FIXDEPS} "${OBJECTDIR}/Source/lcdalfa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -D_DEBUG -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    btl_32MX250F256H_generic.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD3=1,--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   btl_32MX250F256H_generic.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_debug=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Boot2770B.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/debug
	${RM} -r dist/debug

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
