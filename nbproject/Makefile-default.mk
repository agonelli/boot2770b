#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c "Source/MDD file System/FSIO.c" Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o "${OBJECTDIR}/Source/MDD file System/FSIO.o" ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d ${OBJECTDIR}/Source/HWInterface/SPI.o.d ${OBJECTDIR}/Source/HWInterface/HWUART.o.d ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" ${OBJECTDIR}/Source/NVMem.o.d ${OBJECTDIR}/Source/USB/usb_config.o.d ${OBJECTDIR}/Source/USB/usb_host.o.d ${OBJECTDIR}/Source/USB/usb_host_msd.o.d ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d ${OBJECTDIR}/Source/lcdalfa.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o ${OBJECTDIR}/Source/HWInterface/SPI.o ${OBJECTDIR}/Source/HWInterface/HWUART.o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o ${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_config.o ${OBJECTDIR}/Source/USB/usb_host.o ${OBJECTDIR}/Source/USB/usb_host_msd.o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcdalfa.o

# Source Files
SOURCEFILES=Source/FlashDriver/sflashMX25L.c Source/HWInterface/HWTIMER.c Source/HWInterface/HW_FLASH.c Source/HWInterface/SPI.c Source/HWInterface/HWUART.c Source/HWInterface/HWGPIO.c Source/MDD file System/FSIO.c Source/NVMem.c Source/USB/usb_config.c Source/USB/usb_host.c Source/USB/usb_host_msd.c Source/USB/usb_host_msd_scsi.c Source/USB/usb_msd_bootloader.c Source/lcdalfa.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F256H
MP_LINKER_FILE_OPTION=,--script="btl_32MX250F256H_generic.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  .generated_files/flags/default/7724506f70335b15be6cbafd2895dd7d405516b4 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  .generated_files/flags/default/97836bb9bcfc2f4a91c419cb2a7aafcef295a5d1 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  .generated_files/flags/default/986a4ccfa421c8951e8e045f33d7693782dd9f8e .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  .generated_files/flags/default/314271a63ccec61c615ce329b96b62d2de60b1f .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  .generated_files/flags/default/ac7d3394f3c59e4ed9e34bfa0c8f38ed4af2719b .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  .generated_files/flags/default/4dfa8ba225578358f08c5a703eae5e0bbbcee4e1 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  .generated_files/flags/default/dc455cea20fb9ec8183c5dd50df65836c046d672 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/default/6077f553ead39a9fcf9cf9e20661f90d084b7c0a .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  .generated_files/flags/default/ae97e587799779e9885d974c62852720d8d3fbaf .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  .generated_files/flags/default/aa34aa148f980f62d1322dc5625a518f3292a881 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  .generated_files/flags/default/b7e42aaa6665a687053dab3f9e1e386fe442ff53 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  .generated_files/flags/default/ecff9cfe75bc1a3080f20f72694a3b7474d9239a .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/default/a500fd95cefa40fe6fbee1ba2c6720a46b60791a .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  .generated_files/flags/default/29fa864805cb831820856093f59fb8fc1f827cd1 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o: Source/FlashDriver/sflashMX25L.c  .generated_files/flags/default/512035cd3b1be3edcff8b16a9faf6d9b5fca4ef1 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/FlashDriver" 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d 
	@${RM} ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o.d" -o ${OBJECTDIR}/Source/FlashDriver/sflashMX25L.o Source/FlashDriver/sflashMX25L.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWTIMER.o: Source/HWInterface/HWTIMER.c  .generated_files/flags/default/229ce99362a36b3b6fd7201892da0f50bd3635c6 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWTIMER.o Source/HWInterface/HWTIMER.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HW_FLASH.o: Source/HWInterface/HW_FLASH.c  .generated_files/flags/default/d773aa50aa6ef1c54590dac4d3d116ce4fdd2aa6 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HW_FLASH.o.d" -o ${OBJECTDIR}/Source/HWInterface/HW_FLASH.o Source/HWInterface/HW_FLASH.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/SPI.o: Source/HWInterface/SPI.c  .generated_files/flags/default/6804ad72ecdfa727bca7ed3707404fc1b5a7a601 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/SPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/SPI.o.d" -o ${OBJECTDIR}/Source/HWInterface/SPI.o Source/HWInterface/SPI.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWUART.o: Source/HWInterface/HWUART.c  .generated_files/flags/default/c581b3d04b9c46a2699b9ff7965797b7c3e093b5 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWUART.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWUART.o Source/HWInterface/HWUART.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWInterface/HWGPIO.o: Source/HWInterface/HWGPIO.c  .generated_files/flags/default/1d22af6486ea8ee6456c5a75d36ba8a2e591f745 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/HWInterface" 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWInterface/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/HWInterface/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWInterface/HWGPIO.o Source/HWInterface/HWGPIO.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/MDD\ file\ System/FSIO.o: Source/MDD\ file\ System/FSIO.c  .generated_files/flags/default/5de81f59b1c6e8babda56e721a734e07c7f65f0 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/MDD file System" 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o".d 
	@${RM} "${OBJECTDIR}/Source/MDD file System/FSIO.o" 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/MDD file System/FSIO.o.d" -o "${OBJECTDIR}/Source/MDD file System/FSIO.o" "Source/MDD file System/FSIO.c"    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/default/b7ad96504f379364bf1ffd38beb1d5d443137c57 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_config.o: Source/USB/usb_config.c  .generated_files/flags/default/c8eef741d7e77e4aced996a108532aafc9d1ea49 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_config.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_config.o.d" -o ${OBJECTDIR}/Source/USB/usb_config.o Source/USB/usb_config.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host.o: Source/USB/usb_host.c  .generated_files/flags/default/e6889c9d9a51aa51809b12859a268dcb9fd7e4aa .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host.o.d" -o ${OBJECTDIR}/Source/USB/usb_host.o Source/USB/usb_host.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd.o: Source/USB/usb_host_msd.c  .generated_files/flags/default/810a567b850da1940cd28d75116bf233b52f2fd1 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd.o Source/USB/usb_host_msd.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o: Source/USB/usb_host_msd_scsi.c  .generated_files/flags/default/c6038c1e1e495e502457ad6b13f4ac67a839d311 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/Source/USB/usb_host_msd_scsi.o Source/USB/usb_host_msd_scsi.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/default/cc2d3eeb79977f1319c4b31863116a7cdd128942 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcdalfa.o: Source/lcdalfa.c  .generated_files/flags/default/8be4b634adc33499c12f9010c807ed538fa00f6 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o.d 
	@${RM} ${OBJECTDIR}/Source/lcdalfa.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGSCAN_WARNING -DRTC_MANAGE_WEEKDAY -I"Include/USB" -I"Include/MDD File System" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"Z:/msp/SWLIB/CLib" -I"../FILECOMUNI" -MP -MMD -MF "${OBJECTDIR}/Source/lcdalfa.o.d" -o ${OBJECTDIR}/Source/lcdalfa.o Source/lcdalfa.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD3=1,--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	
else
${DISTDIR}/boot2770b.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=1024,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	${MP_CC_DIR}\\xc32-bin2hex ${DISTDIR}/boot2770b.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
